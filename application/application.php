<?php

Map::setPath(array('path' => 'a-panel(/%)', 'folder' => 'a-panel'));
Map::setPath(array('path' => 'login', 'action' => 'login'));
Map::setPath(array('path' => 'register', 'action' => 'register'));
Map::setPath(array('path' => 'logout', 'action' => 'logout'));
Map::setPath(array('path' => 'add-advert', 'action' => 'addAdvert'));
Map::setPath(array('path' => 'advert/\d+', 'action' => 'advert'));
Map::setPath(array('path' => 'load-image', 'action' => 'loadImage'));
Map::setPath(array('path' => 'add-nursery', 'action' => 'addNursery'));
Map::setPath(array('path' => 'nurseries', 'action' => 'nurseries'));
Map::setPath(array('path' => 'nursery/\d+', 'action' => 'nursery'));
Map::setPath(array('path' => 'save-nursery-animal', 'action' => 'saveNurseryAnimal'));
Map::setPath(array('path' => 'facebook', 'action' => 'facebook'));
Map::setPath(array('path' => 'google', 'action' => 'google'));
Map::setPath(array('path' => 'add-user-animal(/\d+)', 'action' => 'addUserAnimal'));
Map::setPath(array('path' => 'user-animals', 'action' => 'userAnimals'));

?>
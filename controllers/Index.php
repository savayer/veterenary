<?php

class IndexController extends Controller {

  public function indexAction() {
    $url = parse_url($_SERVER['REQUEST_URI']);
    if ($url['path'] == '/') {
      return Views::getContent('base2', array('title' => 'דף הבית', 'add_button' => array('add-advert', 'הוסף מודעה'), 'content' => Views::getContent('index', array(
        'adverts' => Models::get('ModelAdvertisement')->getAdverts(array('status' => 1), array(0, 5)),
        'lose_adverts' => Models::get('ModelAdvertisement')->getAdverts(array('type' => 2, 'status' => 1), array(0, 5)),
        'found_adverts' => Models::get('ModelAdvertisement')->getAdverts(array('type' => 1, 'status' => 1), array(0, 5)),
      ))));
    }
    else {
      return Views::getContent('404');
    }
  }

  public function addUserAnimalAction() {
    if ($this->user['id']) {
      if ($this->arg(1)) {
        if ($animal = Models::get('ModelAnimals')->getUserAnimals(array('id' => $this->arg(1)))) {
          list($animal['day'], $animal['month'], $animal['year']) = explode('.', $animal['born']);
          return Views::getContent('base3', array('title' => $animal['name'], 'content' => Views::getContent('add_animal', array('animal' => $animal))));
        }
        else return Views::getContent('404');
      }
      else return Views::getContent('base3', array('title' => 'Add animal', 'content' => Views::getContent('add_animal', array('animal' => array()))));
    }
    else {
      return Views::getContent('403');
    }
  }

  public function userAnimalsAction() {
    if ($this->user['id']) {
      return Views::getContent('base3', array('title' => 'My animals', 'content' => Views::getContent('user_animals', array('animals' => Models::get('ModelAnimals')->getUserAnimals(array('user' => $this->user['id']), array(0, 5))))));
    }
    else {
      return Views::getContent('403');
    }
  }

  public function addNurseryAction() {
    if ($this->user['id']) {
      return Views::getContent('base1', array('title' => 'הרשמה  כעמותה', 'page_name' => '', 'content' => Views::getContent('add_nursery')));
    }
    else {
      return Views::getContent('403');
    }
  }

  public function nurseriesAction() {
    return Views::getContent('base2', array('title' => 'עמותות', 'add_button' => array('add-nursery', 'הוסף  עמותות'), 'content' => Views::getContent('nurseries', array(
      'nurseries' => Models::get('ModelNursery')->getNurseries(array('status' => 1), array(0, 5)),
    ))));
  }

  public function nurseryAction() {
    if ($nursery = Models::get('ModelNursery')->getNurseries(array('id' => $this->arg(1)))) {
      return Views::getContent('base4', array('title' => $nursery['name'], 'header_buttons' => $this->user['id'] == $nursery['user'] ? '<a href="/add-nursery" class="edit">Edit</a><a href="/" class="photo">Photo</a>' : '', 'content' => Views::getContent('nursery', array(
        'nursery' => $nursery,
        'user' => $this->user,
        'animals' => Models::get('ModelNursery')->getAnimals(array('n.id' => $this->arg(1)), array(0, 5))
      ))));
    }
    else {
      return Views::getContent('403');
    }
  }

  public function saveNurseryAnimalAction() {
    $errors = array();
    if (!isset($_POST['kind']) || !is_numeric($_POST['kind']) || !array_key_exists($_POST['kind'], $this->kinds)) $errors['kind'] = 'Wrong kind';
    elseif (!isset($_POST['subspecies']) || !is_numeric($_POST['subspecies']) || !array_key_exists($_POST['subspecies'], $this->subspecies) || $this->subspecies[$_POST['subspecies']]['kind'] != $_POST['kind']) $errors['subspecies'] = 'Wrong subspecies';
    if (!isset($_POST['year']) || !is_numeric($_POST['year']) || !isset($_POST['month']) || !is_numeric($_POST['month']) ||
    !isset($_POST['day']) || !is_numeric($_POST['day']) || !checkdate($_POST['month'], $_POST['day'], $_POST['year'])) $errors['date'] = 'Wrong date';
    if (!isset($_POST['name']) || !is_string($_POST['name']) || !$this->removeScripts(strip_tags($_POST['name']))) $errors['name'] = 'Name is required';
    if (!isset($_POST['nursery']) || !is_numeric($_POST['nursery']) || !($nursery = Models::get('ModelNursery')->getNurseries(array('id' => $_POST['nursery'])))) $errors['nursery'] = 'Wrong nursery';
    elseif ($nursery['user'] != $this->user['id']) $errors['nursery'] = 'You have not rights to add animals to this nursery';
    if (isset($_POST['id']) && (!is_numeric($_POST['id']) || !($animal = Models::get('ModelNursery')->getNurseries(array('id' => $_POST['nursery']))) || $animal['user'] != $this->user['id'])) $errors['nursery'] = 'You have not rights to edit this animal';
    if (isset($_POST['images']) && !is_array($_POST['images'])) $errors['images'] = 'Wrong images';

    if ($errors) {
      echo json_encode(array('type' => 'error', 'errors' => $errors));
    }
    else {
      if (isset($_POST['images'])) {
        foreach ($_POST['images'] as $k => $image) {
          if (!is_string($image) || !file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/' . $image)) unset($_POST['images'][$k]);
        }
        if ($_POST['images']) DB::connect()->query("DELETE FROM uploaded_files WHERE file IN ('" . implode("', '", $_POST['images']) . "')");
        $_POST['images'] = implode(',', $_POST['images']);
      }
      $_POST['born'] = ($_POST['day'] < 10 ? '0' . $_POST['day'] : $_POST['day']) . '.' . ($_POST['month'] < 10 ? '0' . $_POST['month'] : $_POST['month']) . '.' . $_POST['year'];
      $_POST['description'] = isset($_POST['description']) && is_string($_POST['description']) ? $this->removeScripts($_POST['description']) : '';
      $_POST['name'] = $this->removeScripts(strip_tags($_POST['name']));
      $_POST['date'] = time();
      if ($id = Models::get('ModelNursery')->saveAnimal($_POST)) {
        $_POST['id'] = $id;
        if (isset($_POST['images']) && $_POST['images']) $_POST['images'] = explode(',', $_POST['images']);
        else $_POST['images'] = '';
        echo json_encode(array('type' => 'success', 'animal' => $_POST));
      }
    }
    exit;
  }

  public function loadImageAction() {
    if (isset($_POST['image']) && is_string($_POST['image']) && preg_match('/^data:image\/(jpeg|png|gif);base64,/', $_POST['image'])) {
      preg_match_all('/^data:image\/(jpeg|png|gif);base64,/', $_POST['image'], $matches);
      $img = uniqid() . '.' . ($matches[1][0] == 'jpeg' ? 'jpg' : $matches[1][0]);
      file_put_contents($_SERVER['DOCUMENT_ROOT'] . '/files/' . $img, base64_decode(preg_replace('/^data:image\/(?:jpeg|png|gif);base64,/', '', $_POST['image'])));
      DB::connect()->query("INSERT INTO uploaded_files (file, date) values('$img', " . time() . ")");
      echo json_encode(array('status' => 'success', 'image' => $img));
    }
    else {
      echo json_encode(array('status' => 'error', 'message' => 'Wrong data'));
    }
    exit;
  }

  public function advertAction() {
    if ($this->arg(1) && is_numeric($this->arg(1)) && $advert = Models::get('ModelAdvertisement')->getAdverts(array('id' => $this->arg(1)))) {
      return Views::getContent('base3', array('title' => 'Add advert', 'content' => Views::getContent('single_advert', array('advert' => $advert))));
    }
    else {
      return Views::getContent('404');
    }
  }

  public function addAdvertAction() {
    if ($this->user['id']) {
      return Views::getContent('base3', array('title' => 'Add advert', 'content' => Views::getContent('add_advert')));
    }
    else {
      return Views::getContent('403');
    }
  }

  public function loginAction() {
    if ($this->user['id']) $this->redirect('/');
    return Views::getContent('base1', array('content' => Views::getContent('authorization'), 'title' => 'כניסה'));
  }

  public function registerAction() {
    if ($this->user['id']) $this->redirect('/');
    return Views::getContent('base1', array('content' => Views::getContent('register'), 'title' => 'הרשמה'));
  }

  public function logoutAction() {
    if (isset($_SESSION['user'])) unset($_SESSION['user']);
    $this->redirect('/');
  }

  public function facebookAction() {
    if (!$this->user['id']) {
      /* $app_id = '2205328653024448';
      $app_secret = '8b20f6506e30dd95c64aef41dbea7d22'; */
      $app_id = '2106911259371675';
      $app_secret = '0682d5c413c3e5ba4f101e8b6124ebd6';
      $redirect = $this->getSiteUrl() . '/facebook';
      
      if (isset($_GET['code'])) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://graph.Facebook.com/oauth/access_token?client_id=' . $app_id . '&redirect_uri=' . $redirect . '&client_secret=' . $app_secret . '&code=' . $_GET['code']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $res = json_decode(curl_exec($curl));
        curl_close($curl);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://graph.Facebook.com/me?access_token=' . $res->access_token);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $res = json_decode(curl_exec($curl));
        curl_close($curl);

        if (is_object($res) && isset($res->id) && $res->id) {
          /* echo '<pre style="direction:ltr;text-align:left;">' . print_r($res, true) . '</pre>'; */
          $user = DB::connect()->query("SELECT * FROM users WHERE social = 'facebook_" . $res->id . "' OR social LIKE 'facebook_" . $res->id . ",%' OR social LIKE '%,facebook_" . $res->id . ",%' OR social LIKE '%,facebook_" . $res->id . "'")->fetch_assoc();
          if ($user) {
           $required_fields = array('email', 'phone', 'password', 'name', 'specialty');
           foreach ($required_fields as $required_field) {
             if (!isset($user[$required_field]) || !$user[$required_field]) {
               $_SESSION['user_additional_data'] = $user['id'];
               print '<script>
                 window.opener.location = \'/\';
                 window.close();
               </script>';
               exit;
             }
           }
           $this->login($user['id'], '');
           print '<script>
             window.opener.location = \'/\';
             window.close();
           </script>';
          }
          elseif (isset($res->email) && $res->email && $ube = Models::get('ModelsUsers')->getUsers(array('email' => $res->email))) {
           header('Content-Type: text/html; charset=utf-8');
           echo 'האימייל ' . $res->email . ' כבר בשימוש';
          }
          else {
           copy('https://graph.facebook.com/' . $res->id . '/picture?type=large', $_SERVER['DOCUMENT_ROOT'] . '/files/' . ($file_name = uniqid() . 'jpeg'));
           Models::get('ModelUsers')->saveUser(array('name' => $res->name, 'status' => 1, 'email' => isset($res->email) ? $res->email : '', 'register_date' => time(), 'image' => $file_name));
           $id = DB::connect()->insert_id;
           DB::connect()->query("UPDATE users SET social = 'facebook_" . $res->id . "' WHERE id = $id");
           $_SESSION['user_additional_data'] = $id;
           print '<script>
             window.opener.location = \'/\';
             window.close();
           </script>';
          }
        }
        else {
          echo 'Error';
        }
      }
      else {
        $this->redirect('https://www.Facebook.com/dialog/oauth?client_id=' . $app_id . '&redirect_uri=' . $redirect . '&response_type=code&scope=email,user_birthday&display=popup');
      }
    }
    else $this->redirect('/');
  }

  public function googleAction() {
    if (!$this->user['id']) {
      /* $app_id = '589405884796-bf864petsfpbsl0sv860k4d0og39pp29.apps.googleusercontent.com';
      $app_secret = 'wMzuuyxkn4dTu6vBTHtxx0fN'; */
      $app_id = '863438758921-sr3nvld6o03g2j5a1rl0jssrfhc7g3o0.apps.googleusercontent.com';
      $app_secret = '4YGgAE4DgzPJYepGijy83nyH';
      $_SERVER['HTTP_USER_AGENT'] = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";
      $redirect = $this->getSiteUrl() . '/google';

      if (isset($_GET['code'])) {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://accounts.google.com/o/oauth2/token');
        curl_setopt($curl, CURLOPT_POST, TRUE);
        curl_setopt($curl, CURLOPT_POSTFIELDS, 'client_id=' . $app_id . '&client_secret=' . $app_secret . '&redirect_uri=' . $redirect . '&grant_type=authorization_code&code=' . $_GET['code']);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $res = json_decode(curl_exec($curl));
        curl_close($curl);
      
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://www.googleapis.com/oauth2/v1/userinfo?client_id=' . $app_id . '&client_secret=' . $app_secret . '&redirect_uri=' . $redirect . '&grant_type=authorization_code&code=' . $_GET['code'] . '&access_token=' . $res->access_token);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $res = json_decode(curl_exec($curl));
        curl_close($curl);
        
        if (is_object($res) && isset($res->id) && $res->id) {
          $user = DB::connect()->query("SELECT * FROM users WHERE social = 'google_" . $res->id . "' OR social LIKE 'google_" . $res->id . ",%' OR social LIKE '%,google_" . $res->id . ",%' OR social LIKE '%,google_" . $res->id . "'")->fetch_assoc();
          if ($user) {
            //$required_fields = array('email', 'phone', 'password', 'name', 'specialty');
            //foreach ($required_fields as $required_field) {
            //  if (!isset($user[$required_field]) || !$user[$required_field]) {
            //    $_SESSION['user_additional_data'] = $user['id'];
            //    print '<script>
            //      window.opener.location = \'/userAdditionalData\';
            //      window.close();
            //    </script>';
            //    exit;
            //  }
            //}
            $_SESSION['user'] = $user['id'];
            print '<script>
              window.opener.location = \'/\';
              window.close();
            </script>';
          }
          elseif (isset($res->email) && $res->email && $ube = Models::get('ModelUsers')->getUsers(array('email' => $res->email))) {
            header('Content-Type: text/html; charset=utf-8');
            echo 'Email ' . $res->email . ' already in use';
          }
          else {
            //copy($res->picture, $_SERVER['DOCUMENT_ROOT'] . '/files/' . ($file_name = uniqid() . 'jpg'));
            $id = Models::get('ModelUsers')->saveUser(array('name' => $res->name, 'email' => isset($res->email) ? $res->email : '', 'status' => 1));
            DB::connect()->query("UPDATE users SET social = 'google_" . $res->id . "' WHERE id = $id");
            $_SESSION['user'] = $id;
            print '<script>
              window.opener.location = \'/\';
              window.close();
            </script>';
            //print '<script>
            //  window.opener.location = \'/userAdditionalData\';
            //  window.close();
            //</script>';
          }
        }
        else {
          echo 'Error';
        }
      }
      else {
        header('Location: https://accounts.google.com/o/oauth2/auth?redirect_uri=' . $redirect . '&response_type=code&client_id=' . $app_id . '&scope=https://www.googleapis.com/auth/userinfo.email https://www.googleapis.com/auth/userinfo.profile');
      }
    }
    else $this->redirect('/');
  }
}


?>
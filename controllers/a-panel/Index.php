<?php

class IndexController extends Controller {

  public function indexAction() {
    return Views::getContent(isset($_SESSION['admin_login']) ? 'a-panel/base' : 'a-panel/login');
  }

  public function usersAction() {
    if (isset($_SESSION['admin_login'])) {
      $args = array('form' => false);
      if ($this->arg(2) == 'add') {
        $args['form'] = true;
      }
      elseif ($this->arg(2) == 'edit' && is_numeric($this->arg(3)) && $this->arg(3) > 0 && $user = Models::get('ModelUsers')->getUsers(array('id' => $this->arg(3)))) {
        $args['form'] = true;
        $args['user'] = $user;
      }
      elseif ($this->arg(2) == 'delete' && is_numeric($this->arg(3)) && $this->arg(3) > 0 && Models::get('ModelUsers')->getUsers(array('id' => $this->arg(3)))) {
        Models::get('ModelUsers')->deleteUser(array('id' => $this->arg(3)));
        $this->redirect('/a-panel/users');
      }
      else {
        $users = Models::get('ModelUsers')->getUsers();
        $navigation = new Navigation(20, isset($_GET['page']) ? $_GET['page'] : 1, $users);
        $args['materials'] = $navigation->getMaterialsList();
        $args['pager'] = $navigation->getPager();
        $args['total_users'] = count($users);
      }
      return Views::getContent('a-panel/base', array('title' => 'Users', 'content' => Views::getContent('a-panel/users', $args)));
    }
    else return Views::getContent('403');
  }

  public function advertisementsAction() {
    if (isset($_SESSION['admin_login'])) {
      $args = array('form' => false);
      if ($this->arg(2) == 'add') {
        $args['form'] = true;
      }
      elseif ($this->arg(2) == 'edit' && is_numeric($this->arg(3)) && $this->arg(3) > 0 && $advertisement = Models::get('ModelAdvertisement')->getAdverts(array('id' => $this->arg(3)))) {
        $args['form'] = true;
        $args['advertisement'] = $advertisement;
      }
      elseif ($this->arg(2) == 'delete' && is_numeric($this->arg(3)) && $this->arg(3) > 0 && Models::get('ModelAdvertisement')->getAdverts(array('id' => $this->arg(3)))) {
        Models::get('ModelAdvertisement')->deleteAdvert(array('id' => $this->arg(3)));
        $this->redirect('/a-panel/advertisements');
      }
      else {
        $adverts = Models::get('ModelAdvertisement')->getAdverts();
        $navigation = new Navigation(20, isset($_GET['page']) ? $_GET['page'] : 1, $adverts);
        $args['materials'] = $navigation->getMaterialsList();
        $args['pager'] = $navigation->getPager();
        $args['total_adverts'] = count($adverts);
      }
      return Views::getContent('a-panel/base', array('title' => 'Advertisements', 'content' => Views::getContent('a-panel/advertisements', $args)));
    }
    else return Views::getContent('403');
  }

  public function nurseriesAction() {
    if (isset($_SESSION['admin_login'])) {
      $args = array('form' => false);
      if ($this->arg(2) == 'add') {
        $args['form'] = true;
      }
      elseif ($this->arg(2) == 'edit' && is_numeric($this->arg(3)) && $this->arg(3) > 0 && $nursery = Models::get('ModelNursery')->getNurseries(array('id' => $this->arg(3)))) {
        $args['form'] = true;
        $args['nursery'] = $nursery;
      }
      elseif ($this->arg(2) == 'delete' && is_numeric($this->arg(3)) && $this->arg(3) > 0 && Models::get('ModelNursery')->getNurseries(array('id' => $this->arg(3)))) {
        Models::get('ModelNursery')->deleteNursery(array('id' => $this->arg(3)));
        $this->redirect('/a-panel/nurseries');
      }
      else {
        $nurseries = Models::get('ModelNursery')->getNurseries();
        $navigation = new Navigation(20, isset($_GET['page']) ? $_GET['page'] : 1, $nurseries);
        $args['materials'] = $navigation->getMaterialsList();
        $args['pager'] = $navigation->getPager();
        $args['total_nurseries'] = count($nurseries);
      }
      return Views::getContent('a-panel/base', array('title' => 'Nursies', 'content' => Views::getContent('a-panel/nurseries', $args)));
    }
    else return Views::getContent('403');
  }

  public function loginAction() {
    if (!isset($_SESSION['admin_login']) && $_SERVER['REQUEST_METHOD'] == 'POST') {
      if ($_POST['login'] == 'admin' && $_POST['password'] == 'qazwsx') {
        $_SESSION['admin_login'] = true;
        $this->redirect('/a-panel');
      }
      else {
        return Views::getContent('a-panel/login', array('error' => true));
      }
    }
    else {
      $this->redirect('/a-panel');
    }
  }

  public function logoutAction() {
    unset($_SESSION['admin_login']);
    $this->redirect('/a-panel');
  }
}


?>
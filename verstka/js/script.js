$(document).ready(function () {
  window.navigator.getUserMedia({
    video: {
      //facingMode: 'user'
      facingMode: 'environment'
    }
  }, function(stream) {
    var video = document.getElementById('camera-video');
    try {
      video.srcObject = stream;
    }
    catch (ex) {
      video.src = window.URL.createObjectURL(stream);
    }
    video.addEventListener('loadedmetadata', function () {
      centralizeCameraVideo();
      this.play();
    }, false);
  }, function(err) {
    console.error(err);
  });
});
$(window).resize(centralizeCameraVideo);

function centralizeCameraVideo() {
  var video = document.getElementById('camera-video');
  if (video) {
    var width = video.parentNode.parentNode.offsetWidth, height = video.parentNode.parentNode.offsetHeight;
    if (video.videoWidth / width > video.videoHeight / height) {
      video.style.height = height + 'px';
      video.style.width = 'auto';
      video.style.margin = '0 0 0 -' + ((height / video.videoHeight * video.videoWidth - width) / 2) + 'px';
    }
    else {
      video.style.width = width + 'px';
      video.style.height = 'auto';
      video.style.margin = '-' + ((width / video.videoWidth * video.videoHeight - height) / 2) + 'px 0 0 0';
    }
  }
}
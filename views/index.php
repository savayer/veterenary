<?php global $controller; ?>
<!-- <div class="circle-wrapper page-top-circle circle-with-text green-circle">
  <div class="circle">
    <div>
      <div>הכל</div>
    </div>
  </div>
</div> -->
<script>
  document.getElementById('page-name').style.display = 'none';
</script>
<div class="page-info mt--35">
  <div class="tabs-wrapper">
    <ul class="tabs-switcher grey">
      <li>
        <a class="active" href="javascript:void(0)" >הכל</a> <!-- onclick="$('#main > .circle-wrapper > .circle > div > div').html($(this).html());" -->
      </li>
      <li>
        <a href="javascript:void(0)" >אבדו</a> <!-- onclick="$('#main > .circle-wrapper > .circle > div > div').html($(this).html());" -->
      </li>
      <li>
        <a href="javascript:void(0)">נמצאו</a> <!-- onclick="$('#main > .circle-wrapper > .circle > div > div').html($(this).html());" -->
      </li>
    </ul>
    <ul class="tabs-list">
      <li>
        <div class="page-info-inner">
          <?php if ($adverts) : ?>
            <ul class="anilmals-list">
              <?php foreach ($adverts as $advert) : ?>
              <li>
                <a class="anilmals-list-advert" href="/advert/<?php echo $advert['id'] ?>">
                  <div class="animal-image-wrapper">
                    <div class="animal-image" style="background-image: url(<?php echo CropImage::getImage($advert['images'][0]) ?>)"></div>
                  </div>
                  <div class="animal-values">
                    <div class="animal-add-date"><?php echo date('d.m.Y', $advert['date']) ?></div>
                    <div class="animal-value-1 seperated"><?php echo htmlspecialchars($controller->advertisementTypes[$advert['type']]) ?></div>
                    <div class="animal-value-2"><?php echo htmlspecialchars($controller->cutString($advert['description'], 20)) ?></div>
                  </div>
                </a>
              </li>
              <?php endforeach; ?>
            </ul>
          <?php else : ?>
          Nothing found
          <?php endif; ?>
        </div>
      </li>
      <li>
        <div class="page-info-inner">
          <?php if ($lose_adverts) : ?>
            <ul class="anilmals-list">
              <?php foreach ($lose_adverts as $advert) : ?>
              <li>
                <a class="anilmals-list-advert" href="/advert/<?php echo $advert['id'] ?>">
                  <div class="animal-image-wrapper">
                    <div class="animal-image" style="background-image: url(<?php echo CropImage::getImage($advert['images'][0]) ?>)"></div>
                  </div>
                  <div class="animal-values">
                    <div class="animal-add-date"><?php echo date('d.m.Y', $advert['date']) ?></div>
                    <div class="animal-value-1 seperated"><?php echo htmlspecialchars($controller->advertisementTypes[$advert['type']]) ?></div>
                    <div class="animal-value-2"><?php echo htmlspecialchars($controller->cutString($advert['description'], 20)) ?></div>
                  </div>
                </a>
              </li>
              <?php endforeach; ?>
            </ul>
          <?php else : ?>
          Nothing found
          <?php endif; ?>
        </div>
      </li>
      <li>
        <div class="page-info-inner">
          <?php if ($found_adverts) : ?>
            <ul class="anilmals-list">
              <?php foreach ($found_adverts as $advert) : ?>
              <li>
                <a class="anilmals-list-advert" href="/advert/<?php echo $advert['id'] ?>">
                  <div class="animal-image-wrapper">
                    <div class="animal-image" style="background-image: url(<?php echo CropImage::getImage($advert['images'][0]) ?>)"></div>
                  </div>
                  <div class="animal-values">
                    <div class="animal-add-date"><?php echo date('d.m.Y', $advert['date']) ?></div>
                    <div class="animal-value-1 seperated"><?php echo htmlspecialchars($controller->advertisementTypes[$advert['type']]) ?></div>
                    <div class="animal-value-2"><?php echo htmlspecialchars($controller->cutString($advert['description'], 20)) ?></div>
                  </div>
                </a>
              </li>
              <?php endforeach; ?>
            </ul>
          <?php else : ?>
          Nothing found
          <?php endif; ?>
        </div>
      </li>
    </ul>
  </div>
</div>
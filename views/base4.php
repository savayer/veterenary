<?php global $controller; ?>
<!DOCTYPE html>
<html>
  <head>
    <?php echo Views::getContent('head', array('title' => isset($title) ? $title : '')) ?>
  </head>
  <body class="blue-header">
    <div id="wrapper">
      <header>
        <div id="header-inner">
          <?php echo Views::getContent('main_menu', array('user' => $controller->getUser())) ?>
          <ul id="mobile-menu">
            <li></li>
            <li></li>
            <li></li>
          </ul>
          <div id="page-name"><?php echo isset($page_name) ? htmlspecialchars($page_name) : (isset($title) ? htmlspecialchars($title) : ''); ?></div>
          <?php if ($controller->arg(0)) : ?><a href="javascript:history.back()" id="page-back"></a><?php endif; ?>
          <div id="logo">
            <?php if ($controller->arg(0)) : ?><a href="/"><?php endif; ?>
              <img src="/logo.png">
            <?php if ($controller->arg(0)) : ?></a><?php endif; ?>
          </div>
          <?php if (isset($header_buttons)) echo $header_buttons; ?>
        </div>
      </header>
      <div id="main" ><?php echo $content ?></div>
    </div>
  </body>
</html>
<?php global $controller; ?>
<div class="circle-wrapper page-top-circle mt-20 circle-with-text green-circle">
  <div class="circle">
    <div>
      <div>My animals</div>
    </div>
  </div>
</div>
<div class="page-info mt--35">
  <div class="page-info-inner">
    <?php if ($animals) : ?>
      <ul class="anilmals-list">
        <?php foreach ($animals as $animal) : ?>
        <li>
          <a class="anilmals-list-advert" href="/add-user-animal/<?php echo $animal['id'] ?>">
            <div class="animal-image-wrapper">
              <div class="animal-image" style="background-image: url(<?php echo $animal['images'] ? CropImage::getImage($animal['images'][0]) : Controller::DEFAULT_ANIMAL_IMAGE ?>)"></div>
            </div>
            <div class="animal-values">
              <div class="animal-add-date"><?php echo date('d.m.Y', $animal['date']) ?></div>
              <div class="animal-value-1 seperated"><?php echo htmlspecialchars($controller->cutString($animal['name'], 20)) ?></div>
              <div class="animal-value-2"><?php echo htmlspecialchars($controller->cutString($animal['description'], 20)) ?></div>
            </div>
          </a>
        </li>
        <?php endforeach; ?>
      </ul>
    <?php else : ?>
    Nothing found
    <?php endif; ?>
  </div>
</div>
<?php global $controller; ?>
<title><?php if (isset($title)) echo htmlspecialchars($title); ?></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="/css/custom-calendar.css">
<link rel="stylesheet" type="text/css" href="/css/slider.css">
<link rel="stylesheet" type="text/css" href="/css/global.css">
<link rel="stylesheet" type="text/css" href="/css/style.css">
<meta name="viewport" content="width=device-width">
<script src="/js/jquery-3.1.1.min.js"></script>
<!-- <script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&amp;lang=en_US" type="text/javascript"></script> -->
<script src="/js/yandex.js"></script>
<script src="/js/custom-calendar.js"></script>
<script src="/js/slider.js"></script>
<script src="/js/global.js"></script>
<script src="/js/script.js"></script>

<script>
  var kinds = JSON.parse('<?php echo $controller->json_for_js($controller->kinds); ?>'),
  subspecies = JSON.parse('<?php echo $controller->json_for_js($controller->subspecies); ?>'),
  defaultAnimalImage = '<?php echo Controller::DEFAULT_ANIMAL_IMAGE ?>';
</script>
    
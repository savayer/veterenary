<div id="single-animal">
  <?php if (count($advert['images']) > 1) : ?>
  <div class="additional-images">
    <div class="slider-wrapper" visible-slides="2" move-slides="1">
      <div class="slider-left"></div><!--
      --><div class="carousel-wrapper">
        <ul><!--
          <?php foreach ($advert['images'] as $image) : ?>
          --><li>
            <div class="additional-image-wrapper">
              <a href="javascript:void(0)" class="additional-image" style="background-image:url(<?php echo CropImage::getImage($image) ?>)"></a>
            </div>
          </li><!--
          <?php endforeach; ?>
        --></ul>
      </div><!--
      --><div class="slider-right"></div>
    </div>
  </div>
  <?php endif; $location = json_decode($advert['geodefine']); ?>
  <div class="add-image-wrapper circle-wrapper">
    <div class="add-image circle" style="background-image:url(<?php echo CropImage::getImage($advert['images'][0]) ?>)"></div>
  </div>
  <div class="fl-left c-989898 mw-50p p-rel pl-55">
    <a href="<?php echo $location->href; ?>" style="color: #989898; text-decoration: none; font-weight: bold">
    <span class="pin"></span>
    <span>
      <?php 
        echo $location->geo;
      ?>
    </span>
    </a>
  </div>
  <div class="fl-right c-989898 fw-bold mw-50p">
    <?php echo date('d.m.Y',$advert['date']) ; ?>
  </div>
  <div class="clear c-989898 mt-60"><!-- Description --></div><br>
  <div class=""> <!-- page-info-inner -->
    <?php echo htmlspecialchars($advert['description']) ; ?>
  </div>
  <br>
  <div id="chat">
    <ul>
      <li class="current_user">
        <div class="user_data">
          <div class="name">Name</div>
          <div class="time">10:02</div>
        </div>
        <div class="message">hello, where did you see this animal?</div>
      </li>
      <li>
        <div class="user_data">
          <div class="name">Name 2</div>
          <div class="time">10:04</div>
        </div>
        <div class="message">Hi, i saw in Oktyabrskaya avenue, building 38, at 4 o'clock yesterday</div>
      </li>
    </ul>
  </div>
  <br>
  <textarea name="chat_message" class="chat_message" id="" cols="30" rows="3"></textarea>
  <input type="submit" class="blue-button mt-10" value="Save">
</div>
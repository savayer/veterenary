<?php global $controller; ?>
<div class="page-top-circle">
  <div class="circle-wrapper circle-with-text efefef-circle">
    <div class="circle" style="background-image:url(<?php echo $nursery['image'] ? CropImage::getImage($nursery['image']) : Controller::DEFAULT_ANIMAL_IMAGE ?>)"></div>
  </div>
</div>
<div class="page-info mt--15">
  <div class="tabs-wrapper">
    <ul class="tabs-switcher grey">
      <li>
        <a class="active" href="javascript:void(0)">תיאור</a>
      </li>
      <li>
        <a href="javascript:void(0)">חיות מחמד</a>
      </li>
      <li>
        <a href="javascript:void(0)">צור קשר</a>
      </li>
    </ul>
    <ul class="tabs-list">
      <li>
        <div class="page-info-inner with-edit-button"><?php echo $nursery['description'] ?></div>
      </li>
      <li>
        <div class="page-info-inner with-call-button">
          <?php if ($animals) : ?>
          <ul class="anilmals-list-2" id="nursery-animals">
            <?php foreach ($animals as $animal) : ?>
            <li animal-id="<?php echo $animal['id'] ?>">
              <div class="animal-image-wrapper">
                <div class="animal-image" style="background-image: url(<?php echo $animal['images'] ? '/files/' . $animal['images'][0] : Controller::DEFAULT_ANIMAL_IMAGE ?>)"></div>
              </div>
              <div class="animal-value-1"><?php echo htmlspecialchars($animal['name']) ?></div>
              <div class="animal-value-2">
                <a class="edit-button full-rounded nursery-add-animal" animal-data="<?php echo htmlspecialchars(json_encode($animal)) ?>" href="/">Edit</a>
              </div>
            </li>
            <?php endforeach; ?>
          </ul>
          <?php else : ?>
          Nothing found
          <?php endif; ?>
          <?php if ($nursery['user'] == $user['id']) : ?>
          <a href="#" class="plus-button mt-20 nursery-add-animal" nursery="<?php echo $nursery['id'] ?>">Add animal</a>
          <?php endif; ?>
        </div>
      </li>
      <li>
        <div class="page-info-inner with-edit-button"><?php echo $nursery['address'] ?></div>
      </li>
    </ul>
  </div>
</div>
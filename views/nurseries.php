<?php global $controller; ?>
<div class="circle-wrapper page-top-circle circle-with-text green-circle">
  <div class="circle">
    <div>
      <div>הכל</div>
    </div>
  </div>
</div>
<div class="page-info mt--35">
  <div class="page-info-inner">
    <?php if ($nurseries) : ?>
      <ul class="anilmals-list">
        <?php foreach ($nurseries as $nursery) : ?>
        <li>
          <a class="anilmals-list-advert" href="/nursery/<?php echo $nursery['id'] ?>">
            <div class="animal-image-wrapper">
              <div class="animal-image" style="background-image: url(<?php echo $nursery['image'] ? CropImage::getImage($nursery['image']) : Controller::DEFAULT_ANIMAL_IMAGE ?>)"></div>
            </div>
            <div class="animal-values">
              <div class="animal-add-date"><?php echo date('d.m.Y', $nursery['date']) ?></div>
              <div class="animal-value-1 seperated"><?php echo htmlspecialchars($nursery['name']) ?></div>
              <div class="animal-value-2"><?php echo htmlspecialchars($nursery['phone']) ?></div>
            </div>
          </a>
        </li>
        <?php endforeach; ?>
      </ul>
    <?php else : ?>
    Nothing found
    <?php endif; ?>
  </div>
  </div>
</div>
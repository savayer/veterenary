<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <title>404</title>
    <link rel="stylesheet" type="text/css" href="/css/style.css">
  </head>
  <body class="page-not-found">
    <div>404</div>
    <div>Page not found</div>
  </body>
</html>
<?php global $controller; ?>
<!DOCTYPE html>
<html>
  <head>
    <?php echo Views::getContent('head', array('title' => isset($title) ? $title : '')) ?>
  </head>
  <body>
    <div id="wrapper">
      <header>
        <div id="header-inner">
          <?php echo Views::getContent('main_menu', array('user' => $controller->getUser())) ?>
          <ul id="mobile-menu">
            <li></li>
            <li></li>
            <li></li>
          </ul>
          <div id="page-name"><?php echo isset($page_name) ? htmlspecialchars($page_name) : (isset($title) ? htmlspecialchars($title) : ''); ?></div>
          <?php if ($controller->arg(0)) : ?><a href="javascript:history.back()" id="page-back"></a><?php endif; ?>
          <div id="logo">
            <?php if ($controller->arg(0)) : ?><a href="/"><?php endif; ?>
              <img src="/logo.png">
            <?php if ($controller->arg(0)) : ?></a><?php endif; ?>
          </div>
          <div class="text_between_logos"></div>
        </div>
      </header>
      <div id="messages"><?php $controller->getMessages(); ?></div>
      <div id="main" class="add-nursery"><?php echo $content ?></div>
    </div>
  </body>
</html>
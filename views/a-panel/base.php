<!DOCTYPE html>
<html>
  <head>
    <title><?php echo isset($title) ? $title : 'A-panel' ?></title>
    <link rel="stylesheet" type="text/css" href="/css/custom-calendar.css">
    <link rel="stylesheet" type="text/css" href="/css/global.css">
    <link rel="stylesheet" type="text/css" href="/css/a-panel/style.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <script type="text/javascript" src="/js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="/js/tinymce/tinymce.min.js"></script>
    <script src="/js/custom-calendar.js"></script>
    <script type="text/javascript" src="/js/global.js"></script>
    <script type="text/javascript" src="/js/a-panel/script.js"></script>
    <script type="text/javascript">
      tinymce.init({
        selector: '.tinymce',
        plugins: [
          'advlist autolink lists link image charmap print preview hr anchor pagebreak',
          'searchreplace wordcount visualblocks visualchars code fullscreen',
          'insertdatetime media nonbreaking save table contextmenu directionality',
          'emoticons template paste textcolor colorpicker textpattern imagetools'
        ],
        toolbar1: 'ltr rtl | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | fontsizeselect forecolor backcolor emoticons',
        height : '480'
      });
    </script>
  </head>
  <body>
    <div id="top-wrapper">
      <div id="top">
        <div id="site-information">
          <div>Veterenary</div>
          <div>Administration panel</div>
        </div>
        <a id="logout" href="/a-panel/logout">Exit</a>
      </div>
    </div>
    <div id="wrapper">
      <div id="sidebar">
        <ul>
          <li class="header">Sections</li>
          <li>
            <a href="/a-panel/users"<?php if (preg_match('/^\/a-panel\/users/', $_SERVER['REQUEST_URI'])) : ?> class="active"<?php endif; ?>>Users</a>
          </li>
          <li>
            <a href="/a-panel/advertisements"<?php if (preg_match('/^\/a-panel\/advertisements/', $_SERVER['REQUEST_URI'])) : ?> class="active"<?php endif; ?>>Advertisements</a>
          </li>
          <li>
            <a href="/a-panel/nurseries"<?php if (preg_match('/^\/a-panel\/nurseries/', $_SERVER['REQUEST_URI'])) : ?> class="active"<?php endif; ?>>Nurseries</a>
          </li>
        </ul>
      </div>
      <div id="content">
        <?php if (isset($content)) : ?><?php echo $content ?><?php endif; ?>
      </div>
    </div>
  </body>
</html>
<div id="content-wrapper">
  <div id="section-title">
    Users
    <?php if (isset($total_users)) echo '(' . $total_users . ')';  ?>
    <?php if (!$form) : ?><a class="add" href="/a-panel/users/add">Add user</a><?php endif; ?>
  </div>
  <?php if ($form) : Forms::getForm('AdminForm')->render(array('type' => 'users', 'user' => isset($user) ? $user : false)); else : ?>
    <table class="list">
      <thead>
				<th>Id</th>
				<th>Name</th>
				<th>Email</th>
				<th>Actions</th>
      </thead>
      <tbody>
        <?php foreach ($materials as $material) : ?>
        <tr>
          <td><?php echo $material['id'] ?></td>
					<td><?php echo $material['name'] ?></td>
					<td><?php echo $material['email'] ?></td>
          <td>
            <a class="actions" href="/a-panel/users/edit/<?php echo $material['id'] ?>">Edit</a>
            <a class="actions" onclick="return confirm('Delete user &quot;<?php echo $material['email'] ?>&quot;?')" href="/a-panel/users/delete/<?php echo $material['id'] ?>">Delete</a>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <?php if ($pager) : ?>
    <ul class="pager">
      <?php foreach ($pager as $p) : ?>
      <li>
        <a href="/a-panel/users?page=<?php echo $p['link'] ?>"><?php echo $p['num'] ?></a>
      </li> 
      <?php endforeach; ?>
    </ul>
    <?php endif; ?>
  <?php endif; ?>
</div>
<?php global $controller; ?>
<div id="content-wrapper">
  <div id="section-title">
    Advertisements
    <?php if (isset($total_adverts)) echo '(' . $total_adverts . ')';  ?>
    <?php if (!$form) : ?><a class="add" href="/a-panel/advertisements/add">Add advertisements</a><?php endif; ?>
  </div>
  <?php if ($form) : Forms::getForm('AdminForm')->render(array('type' => 'advertisements', 'advertisement' => isset($advertisement) ? $advertisement : false)); else : ?>
    <table class="list">
      <thead>
				<th>Id</th>
				<th>Type</th>
				<th>Phone</th>
        <th>Email</th>
				<th>Actions</th>
      </thead>
      <tbody>
        <?php foreach ($materials as $material) : ?>
        <tr>
          <td><?php echo $material['id'] ?></td>
          <td><?php echo htmlspecialchars($controller->advertisementTypes[$material['type']]) ?></td>
          <td><?php echo $material['phone'] ?></td>
					<td><?php echo $material['email'] ?></td>
          <td>
            <a class="actions" href="/a-panel/advertisements/edit/<?php echo $material['id'] ?>">Edit</a>
            <a class="actions" onclick="return confirm('Delete advertisement id &quot;<?php echo $material['id'] ?>&quot;?')" href="/a-panel/advertisements/delete/<?php echo $material['id'] ?>">Delete</a>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <?php if ($pager) : ?>
    <ul class="pager">
      <?php foreach ($pager as $p) : ?>
      <li>
        <a href="/a-panel/advertisements?page=<?php echo $p['link'] ?>"><?php echo $p['num'] ?></a>
      </li> 
      <?php endforeach; ?>
    </ul>
    <?php endif; ?>
  <?php endif; ?>
</div>
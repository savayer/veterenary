<?php global $controller; ?>
<div id="content-wrapper">
  <div id="section-title">
    Nursies
    <?php if (isset($total_nurseries)) echo '(' . $total_nurseries . ')';  ?>
    <?php if (!$form) : ?><a class="add" href="/a-panel/nurseries/add">Add nursery</a><?php endif; ?>
  </div>
  <?php if ($form) : Forms::getForm('AdminForm')->render(array('type' => 'nurseries', 'nursery' => isset($nursery) ? $nursery : false)); else : ?>
    <table class="list">
      <thead>
				<th>Id</th>
				<th>Name</th>
        <th>Email</th>
				<th>Phone</th>
				<th>Actions</th>
      </thead>
      <tbody>
        <?php foreach ($materials as $material) : ?>
        <tr>
          <td><?php echo $material['id'] ?></td>
					<td><?php echo $material['name'] ?></td>
          <td><?php echo $material['email'] ?></td>
					<td><?php echo $material['phone'] ?></td>
          <td>
            <a class="actions" href="/a-panel/nurseries/edit/<?php echo $material['id'] ?>">Edit</a>
            <a class="actions" onclick="return confirm('Delete nursery id &quot;<?php echo $material['id'] ?>&quot;?')" href="/a-panel/nurseries/delete/<?php echo $material['id'] ?>">Delete</a>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
    <?php if ($pager) : ?>
    <ul class="pager">
      <?php foreach ($pager as $p) : ?>
      <li>
        <a href="/a-panel/nurseries?page=<?php echo $p['link'] ?>"><?php echo $p['num'] ?></a>
      </li> 
      <?php endforeach; ?>
    </ul>
    <?php endif; ?>
  <?php endif; ?>
</div>
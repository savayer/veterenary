<?php global $controller; ?>
<!DOCTYPE html>
<html>
  <head>
    <?php echo Views::getContent('head', array('title' => isset($title) ? $title : '')) ?>
  </head>
  <body class="grey-body with-fixed-footer">
    <div id="wrapper">
      <header>
        <div id="header-inner">
          <?php echo Views::getContent('main_menu', array('user' => $controller->getUser())) ?>
          <ul id="mobile-menu">
            <li></li>
            <li></li>
            <li></li>
          </ul>
          <div id="page-name">
            <form>
              <input type="text">
              <input type="submit">
            </form>
          </div>
          <?php if ($controller->arg(0)) : ?><a href="javascript:history.back()" id="page-back"></a><?php endif; ?>
          <div id="logo">
            <?php if ($controller->arg(0)) : ?><a href="/"><?php endif; ?>
              <img src="/logo.png">
            <?php if ($controller->arg(0)) : ?></a><?php endif; ?>
          </div>
        </div>
      </header>
      <div id="messages"><?php $controller->getMessages(); ?></div>
      <div id="main"><?php echo $content ?></div>
    </div>
    <footer id="fixed-footer">
      <div id="fixed-footer-inner">
        <ul class="fixed-icons-menu">
          <li style="width:19.2%" onmouseover="this.style.width = '23.2%';" onmouseout="this.style.width = '19.2%';">
            <a href="/">
              <div class="icon-wrapper">
                <div class="icon">
                  <div style="background-image:url(/css/img/icon1.png)"></div>
                </div>
              </div>
              <div>Link1</div>
            </a>
          </li>
          <li style="width:19.2%" onmouseover="this.style.width = '23.2%';" onmouseout="this.style.width = '19.2%';">
            <a href="/">
              <div class="icon-wrapper">
                <div class="icon">
                  <div style="background-image:url(/css/img/icon2.png)"></div>
                </div>
              </div>
              <div>Link2</div>
            </a>
          </li>
          <li style="width:19.2%" onmouseover="this.style.width = '23.2%';" onmouseout="this.style.width = '19.2%';">
            <a href="/">
              <div class="icon-wrapper">
                <div class="icon">
                  <div style="background-image:url(/css/img/icon3.png)"></div>
                </div>
              </div>
              <div>Link3</div>
            </a>
          </li>
          <li style="width:19.2%" onmouseover="this.style.width = '23.2%';" onmouseout="this.style.width = '19.2%';">
            <a href="/">
              <div class="icon-wrapper">
                <div class="icon">
                  <div style="background-image:url(/css/img/icon4.png)"></div>
                </div>
              </div>
              <div>Link4</div>
            </a>
          </li>
          <li style="width:19.2%" onmouseover="this.style.width = '23.2%';" onmouseout="this.style.width = '19.2%';">
            <a href="/">
              <div class="icon-wrapper">
                <div class="icon">
                  <div style="background-image:url(/css/img/icon5.png)"></div>
                </div>
              </div>
              <div>Link5</div>
            </a>
          </li>
        </ul>
      </div>
    </footer>
  </body>
</html>
<div id="main-menu-wrapper">
  <!-- <a href="#" id="close-main-menu"></a> -->
  <ul id="main-menu">
    <li>
      <a href="/<?php if ($user['id']) : ?>add-advert<?php else : ?>login<?php endif; ?>">הוסף מודעה</a>
    </li>
    <li>
      <a href="/<?php if ($user['id']) : ?>add-nursery<?php else : ?>login<?php endif; ?>">הוסף  עמותות</a>
    </li>
    <li>
      <a href="/<?php if ($user['id']) : ?>add-user-animal<?php else : ?>login<?php endif; ?>">Add animal</a>
    </li>
    <?php if ($user['id']) : ?>
    <li>
      <a href="/user-animals">My animals</a>
    </li>
    <?php endif; ?>
    <li>
      <a href="/nurseries">עמותות</a>
    </li>
    <?php if ($user['id']) : ?>
    <li>
      <a href="/logout">Logout</a>
    </li>
    <?php endif; ?>
  </ul>
</div>
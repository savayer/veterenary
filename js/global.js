function inArray(n, h) {
  for (var k in h) {
    if (h[k] == n) return true;
  }
  return false;
}
function htmlspecialchars(str) {
  var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
  };
  return str.replace(/[&<>'"]/g, function (m) {return map[m];});
}
var callbackFunctions = {
  pickImages: function () {
    $('.uploaded-images').append('<li><a href="javascript:void(0)"></a><div></div><input type="hidden" name="images[]"></li>');
  },
  uploadImages: function (img) {
    $($('.uploaded-images li:not(.uploaded)')[0]).addClass('uploaded').find('div').css({backgroundImage: 'url(/files/' + img + ')', backgroundSize: 'cover'}).next().val(img);
  },
  errorImages: function () {
    $('.uploaded-images li:not(.uploaded)').remove();
  },
  pickImage: function () {
    $('.uploaded-image').removeAttr('style').removeClass('uploaded').addClass('uploading');
    $('.uploaded-image > input[name="image"][type="hidden"]').val('');
  },
  uploadImage: function (img) {
    $('.uploaded-image').addClass('uploaded').css({backgroundImage: 'url(/files/' + img + ')', backgroundSize: 'cover'});
    $('.uploaded-image > input[name="image"][type="hidden"]').val(img);
  },
  errorImage: function () {
    $('.uploaded-image').removeAttr('style').removeClass('uploading').removeClass('uploaded');
    $('.uploaded-image > input[name="image"][type="hidden"]').val('');
  }
};
$(document).ready(function () {
  $(document).on('click', '.upload-image', function () {
    var filePicker = document.createElement('input'), files = [], button, canPick = true;
    filePicker.type = 'file';
    filePicker.onchange = function () {
      var checkedFiles = 0, num = 0, pickFunc = button.getAttribute('data-function-pick'), uploadFunc = button.getAttribute('data-function-upload'), errorFunc = button.getAttribute('data-function-error'),
      upload = function () {
        if (files.length) {
          var xhr = new XMLHttpRequest();
          xhr.open('POST', '/load-image', true);
          xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
          xhr.onreadystatechange = function () {
            if (this.readyState == 4 && this.status == 200) {
              try {
                var res = JSON.parse(this.responseText);
                if ('status' in res) {
                  if (res.status == 'success') {
                    if (uploadFunc && (uploadFunc in callbackFunctions)) callbackFunctions[uploadFunc](res.image, num);
                    num++;
                    files.splice(0, 1);
                    upload();
                  }
                  else throw ('message' in res) ? [res.message] : ['Something wrong'];
                }
                else throw ['Something wrong'];
              }
              catch (ex) {
                canPick = true;
                files = [];
                if (errorFunc && (errorFunc in callbackFunctions)) callbackFunctions[errorFunc]();
                if (typeof ex == 'object') alert(ex[0]);
              }
            }
          };
          xhr.send('image=' + encodeURIComponent(files[0]));
        }
        else canPick = true;
      },
      checkFiles = function () {
        checkedFiles++;
        if (checkedFiles == filePicker.files.length) {
          filePicker.value = '';
          if (files.length) {
            files.sort(function (a, b) {return a[1] > b[1];});
            files = files.map(function (v) {return v[0];});
            if (pickFunc && (pickFunc in callbackFunctions)) {
              for (var i = 0; i < files.length; i++) {
                callbackFunctions[pickFunc]();
              }
            }
            upload();
          }
          else canPick = true;
        }
      };

      if (('files' in this) && this.files.length) {
        canPick = false;
        for (var i = 0; i < this.files.length; i++) {
          if (inArray(this.files[i].type, ['image/jpeg', 'image/png', 'image/gif'])) {
            (function (fi) {
              var reader = new FileReader();
              reader.onload = function () {
                files.push([this.result, fi]);
                checkFiles();
              };
              reader.readAsDataURL(filePicker.files[fi]);
            })(i);
          }
          else checkFiles();
        }
      }
      else this.value = '';
    };
    return function () {
      if (canPick) {
        button = this;
        if (this.hasAttribute('multiple')) filePicker.multiple = 'multiple';
        else filePicker.removeAttribute('multiple');
        filePicker.click();
      }
      return false;
    };
  }());

  $(document).on('click', '.uploaded-images > li > a', function (ev) {
    $(this).parent().remove();
  });

  $('.uploaded-image > a').click(callbackFunctions.errorImage);
});
function slider(el) {
  var _this = this;
  this.canSlide = true;
  this.el = el;
  this.carouselWrapper = el.getElementsByClassName('carousel-wrapper')[0];
  this.visibleSlides = el.getAttribute('visible-slides') && !isNaN(el.getAttribute('visible-slides')) ? parseFloat(el.getAttribute('visible-slides')) : 2;
  this.moveSlides = el.getAttribute('move-slides') && !isNaN(el.getAttribute('move-slides')) ? parseFloat(el.getAttribute('move-slides')) : 1;
  this.resize();
  el.querySelector('.slider-left').onclick = function () {_this.slideLeft(200);};
  el.querySelector('.slider-right').onclick = function () {_this.slideRight(200);};
}
slider.prototype = {
  resize: function () {
    this.el.getElementsByClassName('carousel-wrapper')[0].style.width = this.el.clientWidth - 40 + 'px';
    var width = this.carouselWrapper.clientWidth / this.visibleSlides, slides = this.carouselWrapper.querySelectorAll('ul:first-child > li');
    this.slideWdith = width;
    this.carouselWrapper.querySelector('ul:first-child').style.width = (width * slides.length) + 'px';
    for (var s = 0; s < slides.length; s++) {
      slides[s].style.width = width + 'px';
    }
  },
  slideLeft: function (time) {
    if (this.canSlide) {
      time = time || 300;
      var _this = this;
      this.canSlide = false;
      var iterations =  time / 10, diff = this.slideWdith / iterations, left = - this.slideWdith, ul = this.carouselWrapper.querySelector('ul:first-child'), func = function () {
        setTimeout(function () {
          left += diff;
          ul.style.left = left + 'px';
          iterations--;
          if (iterations) func();
          else {
            ul.style.left = '0px';
            _this.canSlide = true;
          }
        }, 10);
      };
      ul.style.left = - this.slideWdith + 'px';
      ul.insertBefore(this.carouselWrapper.querySelector('ul:first-child > li:last-child'), ul.querySelector('li'));
      func();
    }
  },
  slideRight: function (time) {
    if (this.canSlide) {
      time = time || 300;
      var _this = this;
      this.canSlide = false;
      var iterations =  time / 10, diff = this.slideWdith / iterations, left = 0, ul = this.carouselWrapper.querySelector('ul:first-child'), func = function () {
        setTimeout(function () {
          left -= diff;
          ul.style.left = left + 'px';
          iterations--;
          if (iterations) func();
          else {
            ul.style.left = '0px';
            ul.appendChild(ul.querySelector('li'));
            _this.canSlide = true;
          }
        }, 10);
      };
      func();
    }
  }
};
function CustomCalendar() {
  this.elements = [];
  this.year = [];
  this.month = [];
  this.settings = [];
  this.months = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
  this.addEvents();
}
CustomCalendar.prototype.getElementNum = function(e) {
  for (var i = 0; i < this.elements.length; i++) {
    if (e == this.elements[i]) {
      return i;
    }
  }
  return 0;
};
CustomCalendar.prototype.addElement = function(element, year, month, settings) {
  var wrapper = document.createElement('div');
  wrapper.setAttribute('class', 'calendar-date');
  element.setAttribute('autocomplete', 'off');
  element.setAttribute('readonly', 'readonly');
  wrapper.innerHTML = element.outerHTML + '<div class="calendar-table"></div>';
  element.parentNode.replaceChild(wrapper, element);
  wrapper.getElementsByTagName('input')[0].onkeydown = function() {
    return false;
  };
  this.elements.push(wrapper.getElementsByTagName('input')[0]);
  this.year.push(year);
  this.month.push(month);
  this.settings.push(settings);
};
CustomCalendar.prototype.getTableCalendar = function(year, month, enabled_dates, disabled_dates, element_date, settings) {
  element_date = element_date ? element_date.split('.') : ['', '', ''];
  var dayInMonth = 35 - new Date(year, month, 35).getDate(),
  first_day = new Date(year, month, 1).getDay() || 7,
  table = '<table><thead><th>Пн</th><th>Вт</th><th>Ср</th><th>Чт</th><th>Пт</th><th>Сб</th><th>Вс</th></thead><tbody>',
  rows = Math.ceil((dayInMonth + first_day - 1) / 7), day = 0, value_day, value_month, new_date = new Date(),
  today_year = new_date.getFullYear(), today_month = new_date.getMonth(), today_day = new_date.getDate();

  for (var r = 0; r < rows; r++) {
    table += '<tr>';
    for (var c = 0; c < 7; c++) {
      if (c + 1 == first_day && day == 0) {
        day = 1;
      }
      if (day > 0 && day <= dayInMonth) {
        value_day = day < 10 ? '0' + day : day;
        value_month = month < 9 ? '0' + (month + 1) : month + 1;
        if (today_year == year && today_month == month && today_day == day) {
          if (enabled_dates) {
            if (enabled_dates.indexOf(value_month + '-' + value_day + '-' + year) === -1) {
              table += '<td class="disabled today" value="' + value_day + '.' + value_month + '.' + year +'">' + day + '</td>';
            }
            else {
              table += '<td class="today" value="' + value_day + '.' + value_month + '.' + year +'">' + day + '</td>';
            }
          }
          else if (disabled_dates && disabled_dates.indexOf(value_month + '-' + value_day + '-' + year) !== -1) {
            table += '<td class="disabled today" value="' + value_day + '.' + value_month + '.' + year +'">' + day + '</td>';
          }
          else {
            table += '<td class="today" value="' + value_day + '.' + value_month + '.' + year +'">' + day + '</td>';
          }
        }
        else if (element_date[2] == year && element_date[1] == value_month && element_date[0] == value_day) {
          if (enabled_dates) {
            if (enabled_dates.indexOf(value_month + '-' + value_day + '-' + year) === -1) {
              table += '<td class="disabled selected" value="' + value_day + '.' + value_month + '.' + year +'">' + day + '</td>';
            }
            else {
              table += '<td class="selected" value="' + value_day + '.' + value_month + '.' + year +'">' + day + '</td>';
            }
          }
          else if ((new_date.getTime() > new Date(year, month, day).getTime() && typeof settings.pastDates != 'undefined' && settings.pastDates === false) || (disabled_dates && disabled_dates.indexOf(value_month + '-' + value_day + '-' + year) !== -1)) {
            table += '<td class="disabled selected" value="' + value_day + '.' + value_month + '.' + year +'">' + day + '</td>';
          }
          else {
            table += '<td class="selected" value="' + value_day + '.' + value_month + '.' + year +'">' + day + '</td>';
          }
        }
        else {
          if (enabled_dates) {
            if (enabled_dates.indexOf(value_month + '-' + value_day + '-' + year) === -1) {
              table += '<td class="disabled" value="' + value_day + '.' + value_month + '.' + year +'">' + day + '</td>';
            }
            else {
              table += '<td value="' + value_day + '.' + value_month + '.' + year +'">' + day + '</td>';
            }
          }
          else if ((new_date.getTime() > new Date(year, month, day).getTime() && typeof settings.pastDates != 'undefined' && settings.pastDates === false) || (disabled_dates && disabled_dates.indexOf(value_month + '-' + value_day + '-' + year) !== -1)) {
            table += '<td class="disabled" value="' + value_day + '.' + value_month + '.' + year +'">' + day + '</td>';
          }
          else {
            table += '<td value="' + value_day + '.' + value_month + '.' + year +'">' + day + '</td>';
          }
        }
        day++;
      }
      else {
        table += '<td></td>';
      }
    }
    table += '</tr>';
  }
  table += '</tbody></table>';
  return table;
};
CustomCalendar.prototype.addEvents = function() {
  var this_object = this;
  $(document).click(function(e) {
    if (e.target.tagName == 'TD' && e.target.getAttribute('value') && e.target.className.indexOf('disabled') === -1) {
      $(e.target).closest('.calendar-date').find('input[type="text"].custom-calendar').val($(e.target).attr('value'));
      $(e.target).closest('.calendar-date').find('input[type="text"].custom-calendar').change();
    }
    if (e.target.className != 'left-month' && e.target.className != 'right-month' && e.target.className.indexOf('disabled') === -1) {
      $('.calendar-table').css('display', 'none');
    }
    else if (e.target.className == 'left-month') {
      var num = this_object.getElementNum(e.target.parentNode.parentNode.parentNode.getElementsByTagName('input')[0]);
      if (this_object.month[num] === 0) {
        this_object.month[num] = 11;
        this_object.year[num] -= 1;
      }
      else {
        this_object.month[num] -= 1;
      }
      var enabled_dates = $(e.target).parent().parent().parent().find('input[type="text"].custom-calendar').attr('enabled-dates'),
      disabled_dates = $(e.target).parent().parent().parent().find('input[type="text"].custom-calendar').attr('disabled-dates'),
      element_date = $(e.target).parent().parent().parent().find('input[type="text"].custom-calendar').val() || '';
      $(e.target).parent().parent().html('<div class="calendar-select-month"><a class="left-month" onclick="return false;"><</a><div class="curent-month">' + this_object.months[this_object.month[num]] + ' ' + this_object.year[num] + '</div><a class="right-month" onclick="return false;">></a></div>' + this_object.getTableCalendar(this_object.year[num], this_object.month[num], enabled_dates, disabled_dates, element_date, this_object.settings[num]));
    }
    else if (e.target.className == 'right-month') {
      var num = this_object.getElementNum(e.target.parentNode.parentNode.parentNode.getElementsByTagName('input')[0]);
      if (this_object.month[num] === 11) {
        this_object.month[num] = 0;
        this_object.year[num] += 1;
      }
      else {
        this_object.month[num] += 1;
      }
      var enabled_dates = $(e.target).parent().parent().parent().find('input[type="text"].custom-calendar').attr('enabled-dates'),
      disabled_dates = $(e.target).parent().parent().parent().find('input[type="text"].custom-calendar').attr('disabled-dates'),
      element_date = $(e.target).parent().parent().parent().find('input[type="text"].custom-calendar').val() || '';
      $(e.target).parent().parent().html('<div class="calendar-select-month"><a class="left-month" onclick="return false;"><</a><div class="curent-month">' + this_object.months[this_object.month[num]] + ' ' + this_object.year[num] + '</div><a class="right-month" onclick="return false;">></a></div>' + this_object.getTableCalendar(this_object.year[num], this_object.month[num], enabled_dates, disabled_dates, element_date, this_object.settings[num]));
    }
    if (e.target.className.indexOf('custom-calendar') !== -1 && e.target.getAttribute('type') == 'text' && e.target.tagName == 'INPUT') {
      var element_number = this_object.getElementNum(e.target),
      element_date = $(e.target).val() || '';
      if (element_date) {
        var element_date_split = element_date.split('.');
        this_object.month[element_number] = parseInt(element_date_split[1]) - 1;
        this_object.year[element_number] = parseInt(element_date_split[2]);
      }
      $(e.target).parent().find('.calendar-table').html('<div class="calendar-select-month"><a class="left-month" onclick="return false;"><</a><div class="curent-month">' + this_object.months[this_object.month[element_number]] + ' ' + this_object.year[element_number] + '</div><a class="right-month" onclick="return false;">></a></div>' + this_object.getTableCalendar(this_object.year[element_number], this_object.month[element_number], $(e.target).attr('enabled-dates'), $(e.target).attr('disabled-dates'), element_date, this_object.settings[element_number]));
      e.target.parentNode.getElementsByClassName('calendar-table')[0].style.display = 'block';
    }
  });
};
CustomCalendar.init = function(element, settings) {
  if (typeof element.tagName != 'undefined' && element.tagName == 'INPUT' && typeof element.type != 'undefined' && element.type == 'text') {
    if (typeof CustomCalendar.calendar == 'undefined' || typeof CustomCalendar.calendar.constructor.name == 'undefined' || CustomCalendar.calendar.constructor.name != 'CustomCalendar') {
      CustomCalendar.calendar = new CustomCalendar();
    }
    var year, month, date = new Date();
    if (typeof element.value != 'undefined' && element.value) {
      var default_date = element.value.split('.');
      if (default_date.length == 3 && new Date(default_date[1] + '-' + default_date[0] + '-' + default_date[2]) != 'Invalid Date') {
        year = default_date[2];
        month = default_date[1] - 1;
      }
    }
    if (typeof year == 'undefined' || typeof month == 'undefined') {
      year = date.getFullYear();
      month = date.getMonth();
    }
    CustomCalendar.calendar.addElement(element, year, month, typeof settings != 'undefined' ? settings : {});
  }
};
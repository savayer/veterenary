$(document).ready(function () {
  $('.dynamic-list-add').click(function () {
    $(this).parent().find('ul').append('<li><input type="text" name="' + $(this).parent().attr('name') + '"><a class="dynamic-list-remove" href="javascript:void(0)">Remove</a></li>');
  });
  $(document).on('click', '.dynamic-list-remove', function () {
    $(this).parent().remove();
  });

  $('.custom-calendar').each(function () {
    CustomCalendar.init(this, {pastDates: true});
  });
});
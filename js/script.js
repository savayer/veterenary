function popup(content, style) {
  if ($('#popup-wrapper').length) {
    $('#popup').html(content);
  } else {
    $('body').append('<div id="popup-wrapper"><a href="javascript:void(0)" id="close-popup"></a><div id="popup">' + content + '</div></div>');
    $('#close-popup').click(function () {
      $('#popup-wrapper').remove();
    });
  }
  $('#popup').removeAttr('style');
  if (style) $('#popup').css(style);
}


/* camera */
window.onload = function () {
/*   $('.geolocate').text(ymaps.geolocation.city);
  $('input[name=geodefine]').val(ymaps.geolocation.city);
 */
  // References to all the element we will need.
  var video = document.querySelector('#camera-stream'),
    image = document.querySelector('#snap'),
    start_camera = document.querySelector('.edit-animal-image--upload-image'),
    controls = document.querySelector('.controls'),
    take_photo_btn = document.querySelector('#take-photo'),
    delete_photo_btn = document.querySelector('#delete-photo'),
    download_photo_btn = document.querySelector('#download-photo'),
    error_message = document.querySelector('#error-message'),
    closeCamera = document.getElementById('close_camera'),
    switchCamera = document.getElementById('switch_camera'),
    select = document.getElementById('cameras'),
    currentStream;

  function gotDevices(mediaDevices) {
    select.innerHTML = '';
    select.appendChild(document.createElement('option'));
    let count = 1;
    mediaDevices.forEach(mediaDevice => {
      if (mediaDevice.kind === 'videoinput') {
        const option = document.createElement('option');
        option.value = mediaDevice.deviceId;
        const label = mediaDevice.label || `Camera ${count  }`;
        const textNode = document.createTextNode(label);
        option.appendChild(textNode);
        select.appendChild(option);
      }
    });
  }

  navigator.mediaDevices.enumerateDevices().then(gotDevices);

  closeCamera.addEventListener('click', function (e) {
    e.preventDefault();
    hideUI();
    $('.camera').fadeOut();
  })

  start_camera.addEventListener("click", function (e) {

    e.preventDefault();
    if (typeof currentStream !== 'undefined') {
      stopMediaTracks(currentStream);
    }
    const videoConstraints = {};
    if (select.value === '') {
      videoConstraints.facingMode = 'environment';
    } else {
      videoConstraints.deviceId = {
        exact: select.value
      };
    }
    const constraints = {
      video: videoConstraints,
      audio: false
    };

    navigator.mediaDevices
      .getUserMedia(constraints)
      .then(stream => {
        currentStream = stream;
        video.srcObject = stream;
        return navigator.mediaDevices.enumerateDevices();
      })
      .then(gotDevices)
      .catch(error => {
        console.error(error);
      });

    $('.camera').fadeIn();
    video.play();
    showVideo();
    // The getUserMedia interface is used for handling camera input.
    // Some browsers need a prefix so here we're covering all the options
    /*     navigator.getMedia = (navigator.getUserMedia ||
          navigator.webkitGetUserMedia ||
          navigator.mozGetUserMedia ||
          navigator.msGetUserMedia);


        if (navigator.getMedia) {

          // Request the camera.
          navigator.getMedia({
              video: true
            },
            // Success Callback
            function (stream) {

              // Create an object URL for the video stream and
              // set it as src of our HTLM video element.
              video.src = window.URL.createObjectURL(stream);

              // Play the video element to start the stream.
              video.play();
              video.onplay = function () {
                showVideo();
              };

            },
            // Error Callback
            function (err) {
              displayErrorMessage("There was an error with accessing the camera stream: " + err.name, err);
            }
          );

        }
        // Start video playback manually.
        video.play();
        showVideo();
     */
  });
  function stopMediaTracks(stream) {
    stream.getTracks().forEach(track => {
        track.stop();
    });
  }
  download_photo_btn.addEventListener('click', function () {
    callbackFunctions.pickImages();

    var xhr = new XMLHttpRequest();
    xhr.open('POST', '/load-image', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        try {
          var res = JSON.parse(this.responseText);
          if ('status' in res) {
            if (res.status == 'success') {
              if ('uploadImages' && ('uploadImages' in callbackFunctions)) callbackFunctions.uploadImages(res.image, 0);
            } else throw ('message' in res) ? [res.message] : ['Something wrong'];
          } else throw ['Something wrong'];
        } catch (ex) {
          canPick = true;
          files = [];
          if ('errorImages' && ('errorImages' in callbackFunctions)) callbackFunctions.errorImages();
          if (typeof ex == 'object') alert(ex[0]);
        }
      }
    };
    xhr.send('image=' + encodeURIComponent($('#snap').attr('src')));

    hideUI();
  })

  take_photo_btn.addEventListener("click", function (e) {

    e.preventDefault();

    var snap = takeSnapshot();

    // Show image. 
    image.setAttribute('src', snap);
    image.classList.add("visible");

    // Enable delete and save buttons
    delete_photo_btn.classList.remove("disabled");
    download_photo_btn.classList.remove("disabled");

    // Set the href attribute of the download button to the snap url.
    download_photo_btn.href = snap;

    // Pause video playback of stream.
    video.pause();

  });


  delete_photo_btn.addEventListener("click", function (e) {

    e.preventDefault();

    // Hide image.
    image.setAttribute('src', "");
    image.classList.remove("visible");

    // Disable delete and save buttons
    delete_photo_btn.classList.add("disabled");
    download_photo_btn.classList.add("disabled");

    // Resume playback of stream.
    video.play();

  });



  function showVideo() {
    // Display the video stream and the controls.

    hideUI();
    video.classList.add("visible");
    controls.classList.add("visible");
  }


  function takeSnapshot() {
    // Here we're using a trick that involves a hidden canvas element.  

    var hidden_canvas = document.querySelector('canvas'),
      context = hidden_canvas.getContext('2d');

    var width = video.videoWidth,
      height = video.videoHeight;

    if (width && height) {

      // Setup a canvas with the same dimensions as the video.
      hidden_canvas.width = width;
      hidden_canvas.height = height;

      // Make a copy of the current frame in the video on the canvas.
      context.drawImage(video, 0, 0, width, height);

      // Turn the canvas image into a dataURL that can be used as a src for our photo.
      return hidden_canvas.toDataURL('image/png');
    }
  }


  function displayErrorMessage(error_msg, error) {
    error = error || "";
    if (error) {
      console.log(error);
    }

    error_message.innerText = error_msg;

    hideUI();
    error_message.classList.add("visible");
  }


  function hideUI() {
    // Helper function for clearing the app UI.

    controls.classList.remove("visible");
    start_camera.classList.remove("visible");
    video.classList.remove("visible");
    snap.classList.remove("visible");
    error_message.classList.remove("visible");
  }

}

$(document).ready(function () {
  $('#mobile-menu').click(function () {
    $('#main-menu-wrapper').toggleClass('opened');
  });
  $('#close-main-menu').click(function () {
    $('#main-menu-wrapper').removeClass('opened');
    return false;
  });

  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (pos) {
      //console.log(pos);
      var latitude = pos.coords.latitude,
          longitude = pos.coords.longitude,
          sendInterval;
      $('.geolocate').attr('href', 'https://www.google.com/maps/?q='+latitude+','+longitude);
      $('.geolocate').css('text-decoration', 'none');
      $.ajax({
        method: 'get',
        url: 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+latitude+','+longitude+'&key=AIzaSyCKhTCc4ZBedeKMvHCB7tnapAS7QfuJH9I',        
      }).done(function(data) {
        if (data.status == 'OK') {
          clearInterval(sendInterval);
          var address = data.results[0];
          $('.geolocate').text(address.formatted_address);
          $('#preloader-image').hide();
          var data_define = {
            geo: address.formatted_address,
            href: 'https://www.google.com/maps/?q='+latitude+','+longitude
          }
          $('input[name=geodefine]').val(JSON.stringify(data_define));
        } else {
          $('.geolocation').text('location is not defined');
        }      
      })
      //alert(pos);
    });
  } else {
    $('.geolocate').text('Location is not defined');
  }
  /* $('.geolocate').click(function () {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (pos) {
        console.log(pos);
        //alert(pos);
      });
    }
    return false;
  }); */

  $('.tabs-switcher > li > a').each(function (num) {
    $(this).click(function () {
      $(this).closest('.tabs-switcher').find('a').removeClass('active');
      $($(this).addClass('active').closest('.tabs-wrapper').find('.tabs-list').children().css('display', 'none').parent().children()[num]).css('display', 'block');
    });
  });

  $('.custom-calendar').each(function () {
    CustomCalendar.init(this, {
      pastDates: true
    });
  });

  $('.slider-wrapper').each(function () {
    var s = new slider(this);
    $(window).resize(s.resize);
  });

  $('.additional-image').click(function () {
    $('#single-animal > .add-image-wrapper > .add-image').css('background-image', this.style['background-image']);
  });

  $(document).on('click', '.nursery-add-animal', function () {
    var animal = {};
    try {
      if (!this.hasAttribute('animal-data')) throw '';
      animal = JSON.parse(this.getAttribute('animal-data'));
      if ('born' in animal) animal.splitedBorn = animal.born.split('.');
    } catch (ex) {}
    var html = '<form id="add-animal-to-nursery">'
    html += '<div id="make-photo-wrapper">';
    html += ' <div class="add-image-wrapper circle-wrapper">';
    html += '   <div class="add-image circle" style="background-image: url(' + defaultAnimalImage + ');">';
    html += '     <video id="camera-video" style="width: 214px; height: auto;"></video>';
    html += '   </div>';
    html += ' </div>';
    html += '</div>';
    html += '<a href="#" class="upload-image" multiple="" data-function-pick="pickImages" data-function-upload="uploadImages" data-function-error="errorImages">Add image</a>';
    html += '<div class="animal-data">';
    html += ' <ul class="uploaded-images mt-20 ta-right" error-target="images">';
    if ('images' in animal && animal.images) {
      for (var i = 0; i < animal.images.length; i++) {
        html += '<li class="uploaded"><a href="javascript:void(0)"></a><div style="background-image:url(/files/' + animal.images[i] + ');background-size:cover"></div><input type="hidden" name="images[]" value="' + animal.images[i] + '"></li>';
      }
    }
    html += ' </ul>';
    html += ' <h3>Some text</h3>';
    html += ' <div class="animal-kind" error-target="kind">';
    for (var k in kinds) {
      html += '<label>';
      html += '  <input type="radio" name="kind" value="' + k + '" required' + ('kind' in animal && animal.kind == k ? ' checked' : '') + '>';
      html += '  <span style="background-image: url(' + kinds[k].img + ')"></span>';
      html += '</label>';
    }
    html += ' </div>';
    html += ' <h3>Animal birthday</h3>';
    html += ' <div class="animal-birthday" error-target="date">';
    html += '   <select name="year">';
    for (var year = (new Date()).getFullYear(), y = year; y >= year - 20; y--) {
      html += '<option value="' + y + '"' + ('splitedBorn' in animal && animal.splitedBorn[2] == y ? ' selected' : '') + '>' + y + '</option>';
    }
    html += '   </select>';
    html += '   <select name="month">';
    for (var m = 1; m <= 12; m++) {
      html += '<option value="' + m + '"' + ('splitedBorn' in animal && animal.splitedBorn[1] == m ? ' selected' : '') + '>' + (m < 10 ? '0' + m : m) + '</option>';
    }
    html += '   </select>';
    html += '   <select name="day">';
    for (d = 1; d <= 31; d++) {
      html += '<option value="' + d + '"' + ('splitedBorn' in animal && animal.splitedBorn[0] == d ? ' selected' : '') + '>' + (d < 10 ? '0' + d : d) + '</option>';
    }
    html += '   </select>';
    html += ' </div>';
    html += ' <h3>Animal name</h3>';
    html += ' <div error-target="name">';
    html += '   <input type="text" name="name" required' + ('name' in animal ? ' value="' + htmlspecialchars(animal.name) + '"' : '') + '>';
    html += ' </div>';
    html += ' <div class="mt-20">';
    html += '   <textarea name="description" placeholder="Description">' + ('description' in animal ? htmlspecialchars(animal.description) : '') + '</textarea>';
    html += ' </div>';
    html += ' <h3 class="animal-subspecies-select"' + ('subspecies' in animal ? ' style="display:block"' : '') + '>Some select</h3>';
    html += ' <div class="long-select animal-subspecies-select" error-target="subspecies"' + ('subspecies' in animal ? ' style="display:block"' : '') + '>';
    html += '   <select name="subspecies">';
    for (var s in subspecies) {
      html += '<option value="' + s + '" kind="' + subspecies[s].kind + '"' + (!('kind' in animal) || animal.kind != subspecies[s].kind ? ' style="display:none;"' : '') + ('subspecies' in animal && animal.subspecies == s ? ' selected' : '') + '>' + htmlspecialchars(subspecies[s].name) + '</option>';
    }
    html += '   </select>';
    html += ' </div>';
    html += '</div>';
    html += '<div error-target="nursery">';
    html += ' <input type="hidden" name="nursery" value="' + ('nursery' in animal ? animal.nursery : this.getAttribute('nursery')) + '">';
    if ('id' in animal) html += ' <input type="hidden" name="id" value="' + animal.id + '">';
    html += '</div>';
    html += '<div class="scrobler"></div>';
    html += '<input type="submit" class="button w-100p" value="Submit">';
    html += '</form>';
    popup(html);
    return false;
  });

  $(document).on('change', 'input[type="radio"][name="kind"]', function () {
    var race = $('select[name="subspecies"] option[kind="' + htmlspecialchars(this.value) + '"]');
    $('.animal-subspecies-select').css('display', 'block');
    $('select[name="subspecies"] option').css('display', 'none');
    $('select[name="subspecies"] option[kind="' + htmlspecialchars(this.value) + '"]').css('display', 'block');
    $('select[name="subspecies"]').val(race.length ? race[0].value : '');
  });

  $(document).on('submit', '#add-animal-to-nursery', function () {
    if (!$(this).hasClass('no-submit')) {
      var _this = this;
      $(this).addClass('no-submit');
      $('#add-animal-to-nursery .error').remove();
      $.ajax({
        url: '/save-nursery-animal',
        method: 'post',
        data: $(this).serialize(),
        success: function (res) {
          try {
            var r = JSON.parse(res);
            if (!('type' in r) || !inArray(r.type, ['error', 'success'])) throw '';
            if (r.type == 'success') {
              if ($('[animal-id="' + r.animal.id + '"]').length) {
                $('[animal-id="' + r.animal.id + '"] .animal-image').css('background-image', 'url(' + (r.animal.images ? '/files/' + r.animal.images[0] : defaultAnimalImage) + ')');
                $('[animal-id="' + r.animal.id + '"] .animal-value-1').html(r.animal.name);
                $('[animal-id="' + r.animal.id + '"] .animal-value-2 a').attr('animal-data', JSON.stringify(r.animal));
              } else {
                var html = '<li animal-id="' + r.animal.id + '">';
                html += ' <div class="animal-image-wrapper">';
                html += '   <div class="animal-image" style="background-image: url(' + (r.animal.images ? '/files/' + r.animal.images[0] : defaultAnimalImage) + ')"></div>';
                html += ' </div>';
                html += ' <div class="animal-value-1">' + htmlspecialchars(r.animal.name) + '</div>';
                html += ' <div class="animal-value-2">';
                html += '   <a class="edit-button full-rounded nursery-add-animal" animal-data="' + htmlspecialchars(JSON.stringify(r.animal)) + '" href="/">Edit</a>';
                html += ' </div>';
                html += '</li>';
                if ($('#nursery-animals > li').length) $(html).insertBefore($('#nursery-animals > li:first-child'));
                else $('#nursery-animals').append(html);
              }
              $('#popup-wrapper').remove();
            } else {
              for (var er in r.errors) {
                $('#add-animal-to-nursery [error-target="' + er + '"]').append('<div class="error">' + r.errors[er] + '</div>');
              }
              $(_this).removeClass('no-submit');
            }
          } catch (ex) {
            popup('Something wrong', {
              background: '#fff',
              padding: '10px',
              borderRadius: '10px',
              textAlign: 'center'
            });
          }
        }
      });
    }
    return false;
  });

  $('.social-login').click(function () {
    var width = 450,
      height = 450,
      left = (window.innerWidth - width) / 2,
      top = (window.innerHeight - height) / 2;
    popup_window = window.open(this.href, this.getAttribute('id'), 'top=' + top + ',left=' + left + ',location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,titlebar=yes,toolbar=no,channelmode=yes,fullscreen=yes,width=' + width + ',height=' + height);
    popup_window.focus();
    return false;
  });
});
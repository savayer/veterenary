<?php

class ModelUsers extends Models {

  public function getUsers($args = array()) {
    $users = array();
    $result = DB::connect()->query("SELECT * FROM users" . ($args ? ' WHERE ' . implode(' AND ', array_map(function ($k, $v) {return $k . (is_array($v) ? ' IN (' . implode(', ', array_map(function ($a) {return "'" . addslashes($a) . "'";}, $v)) . ')' : " = '" . addslashes($v) . "'");}, array_keys($args), $args)) : '') . " ORDER BY id DESC");
    while ($res = $result->fetch_assoc()) {
      $users[$res['id']] = $res;
    }
    return (isset($args['id']) && is_numeric($args['id'])) || (isset($args['email']) && is_string($args['email'])) ? ($users ? current($users) : false) : $users;
  }

  public function saveUser($data) {
    global $controller;
    $fields = array('id', 'name', 'email', 'phone', 'address', 'password', 'status');
    $query = array('fields' => array(), 'values' => array(), 'update' => array());
    foreach ($fields as $field) {
      if (isset($data[$field])) {
        $query['fields'][] = $field;
        $query['values'][] = "'" . ($field == 'password' ? md5($data[$field] . $controller->password_salt) : addslashes($data[$field])) . "'";
        if ($field != 'id') $query['update'][] = "$field = VALUES($field)";
      }
    }
    DB::connect()->query("INSERT INTO users (" . implode(', ', $query['fields']) . ") VALUES (" . implode(', ', $query['values']) . ") ON DUPLICATE KEY UPDATE " . implode(', ', $query['update']));
    return DB::connect()->error ? false : (isset($data['id']) ? $data['id'] : DB::connect()->insert_id);
  }

  public function deleteUser($args) {
    DB::connect()->query("DELETE FROM users WHERE " . implode(' AND ', array_map(function ($k, $v) {return $k . (is_array($v) ? ' IN (' . implode(', ', array_map(function ($a) {return "'" . addslashes($a) . "'";}, $v)) . ')' : " = '" . addslashes($v) . "'");}, array_keys($args), $args)));
  }
}

?>
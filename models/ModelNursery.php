<?php

class ModelNursery extends Models {

  public function getNurseries($args = array(), $limit = array()) {
    $nurseries = array();
    $result = DB::connect()->query("SELECT * FROM nurseries" . ($args ? ' WHERE ' . implode(' AND ', array_map(function ($k, $v) {return $k . (is_array($v) ? ' IN (' . implode(', ', array_map(function ($a) {return "'" . addslashes($a) . "'";}, $v)) . ')' : " = '" . addslashes($v) . "'");}, array_keys($args), $args)) : '') . ' ORDER BY id DESC' . ($limit ? ' LIMIT ' . $limit[0] . ', '. $limit[1] : ''));
    while ($res = $result->fetch_assoc()) {
      $nurseries[$res['id']] = $res;
    }
    return (isset($args['id']) && is_numeric($args['id'])) ? ($nurseries ? current($nurseries) : false) : $nurseries;
  }

  public function saveNursery($data) {
    global $controller;
    $fields = array('id', 'name', 'email', 'phone', 'address', 'description', 'image', 'date', 'user', 'status');
    $query = array('fields' => array(), 'values' => array(), 'update' => array());
    foreach ($fields as $field) {
      if (isset($data[$field])) {
        $query['fields'][] = $field;
        $query['values'][] = "'" . addslashes($data[$field]) . "'";
        if ($field != 'id') $query['update'][] = "$field = VALUES($field)";
      }
    }
    DB::connect()->query("INSERT INTO nurseries (" . implode(', ', $query['fields']) . ") VALUES (" . implode(', ', $query['values']) . ") ON DUPLICATE KEY UPDATE " . implode(', ', $query['update']));
    return DB::connect()->error ? false : (isset($data['id']) ? $data['id'] : DB::connect()->insert_id);
  }

  public function deleteNursery($args) {
    DB::connect()->query("DELETE FROM nurseries WHERE " . implode(' AND ', array_map(function ($k, $v) {return $k . (is_array($v) ? ' IN (' . implode(', ', array_map(function ($a) {return "'" . addslashes($a) . "'";}, $v)) . ')' : " = '" . addslashes($v) . "'");}, array_keys($args), $args)));
  }

  public function getAnimals($args = array(), $limit = array()) {
    $nurseries = array();
    $result = DB::connect()->query("SELECT na.*, n.user FROM nursery_animals na INNER JOIN nurseries n ON n.id = na.nursery" . ($args ? ' WHERE ' . implode(' AND ', array_map(function ($k, $v) {return $k . (is_array($v) ? ' IN (' . implode(', ', array_map(function ($a) {return "'" . addslashes($a) . "'";}, $v)) . ')' : " = '" . addslashes($v) . "'");}, array_keys($args), $args)) : '') . ' ORDER BY id DESC' . ($limit ? ' LIMIT ' . $limit[0] . ', '. $limit[1] : ''));
    while ($res = $result->fetch_assoc()) {
      $res['images'] = $res['images'] ? explode(',', $res['images']) : '';
      $nurseries[$res['id']] = $res;
    }
    return (isset($args['id']) && is_numeric($args['id'])) ? ($nurseries ? current($nurseries) : false) : $nurseries;
  }

  public function saveAnimal($data) {
    global $controller;
    $fields = array('id', 'name', 'images', 'kind', 'subspecies', 'born', 'description', 'nursery', 'date');
    $query = array('fields' => array(), 'values' => array(), 'update' => array());
    foreach ($fields as $field) {
      if (isset($data[$field])) {
        $query['fields'][] = $field;
        $query['values'][] = "'" . addslashes($data[$field]) . "'";
        if ($field != 'id') $query['update'][] = "$field = VALUES($field)";
      }
    }
    DB::connect()->query("INSERT INTO nursery_animals (" . implode(', ', $query['fields']) . ") VALUES (" . implode(', ', $query['values']) . ") ON DUPLICATE KEY UPDATE " . implode(', ', $query['update']));
    return DB::connect()->error ? false : (isset($data['id']) ? $data['id'] : DB::connect()->insert_id);
  }
}

?>
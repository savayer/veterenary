<?php

class ModelAdvertisement extends Models {

  public function getAdverts($args = array(), $limit = array()) {
    $adverts = array();
    $result = DB::connect()->query("SELECT * FROM advertisement" . ($args ? ' WHERE ' . implode(' AND ', array_map(function ($k, $v) {return $k . (is_array($v) ? ' IN (' . implode(', ', array_map(function ($a) {return "'" . addslashes($a) . "'";}, $v)) . ')' : " = '" . addslashes($v) . "'");}, array_keys($args), $args)) : '') . ' ORDER BY id DESC' . ($limit ? ' LIMIT ' . $limit[0] . ', '. $limit[1] : ''));
    while ($res = $result->fetch_assoc()) {
      $location = json_decode($res['location']);
      $user_data = json_decode($res['user_data']);
      $city = $location->city ? Models::get('ModelCities')->getCities(array('id' => $location->city)) : false;
      $res['images'] = explode(',', $res['images']);
      $res['city'] = $location->city;
      $res['city_name'] = $city ? $city['name'] : '';
      $res['street'] = $location->street;
      $res['house_number'] = $location->house_number;
      $res['event_date'] = $location->event_date;
      $res['name'] = $user_data->name;
      $res['phone'] = $user_data->phone;
      $res['email'] = $user_data->email;
      $adverts[$res['id']] = $res;
    }
    return (isset($args['id']) && is_numeric($args['id'])) ? ($adverts ? current($adverts) : false) : $adverts;
  }

  public function saveAdvert($data) {
    global $controller;
    $fields = array('id', 'type', 'images', 'location', 'geodefine', 'user_data', 'description', 'date', 'user', 'status');
    $query = array('fields' => array(), 'values' => array(), 'update' => array());
    foreach ($fields as $field) {
      if (isset($data[$field])) {
        $query['fields'][] = $field;
        $query['values'][] = "'" . addslashes($data[$field]) . "'";
        if ($field != 'id') $query['update'][] = "$field = VALUES($field)";
      }
    }
    DB::connect()->query("INSERT INTO advertisement (" . implode(', ', $query['fields']) . ") VALUES (" . implode(', ', $query['values']) . ") ON DUPLICATE KEY UPDATE " . implode(', ', $query['update']));
    return DB::connect()->error ? false : (isset($data['id']) ? $data['id'] : DB::connect()->insert_id);
  }

  public function deleteAdvert($args) {
    DB::connect()->query("DELETE FROM advertisement WHERE " . implode(' AND ', array_map(function ($k, $v) {return $k . (is_array($v) ? ' IN (' . implode(', ', array_map(function ($a) {return "'" . addslashes($a) . "'";}, $v)) . ')' : " = '" . addslashes($v) . "'");}, array_keys($args), $args)));
  }
}

?>
<?php

class ModelAnimals extends Models {

  public function getUserAnimals($args = array(), $limit = array()) {
    $animals = array();
    $result = DB::connect()->query("SELECT * FROM user_animals" . ($args ? ' WHERE ' . implode(' AND ', array_map(function ($k, $v) {return $k . (is_array($v) ? ' IN (' . implode(', ', array_map(function ($a) {return "'" . addslashes($a) . "'";}, $v)) . ')' : " = '" . addslashes($v) . "'");}, array_keys($args), $args)) : '') . ' ORDER BY id DESC' . ($limit ? ' LIMIT ' . $limit[0] . ', '. $limit[1] : ''));
    while ($res = $result->fetch_assoc()) {
      $res['images'] = $res['images'] ? explode(',', $res['images']) : '';
      $animals[$res['id']] = $res;
    }
    return (isset($args['id']) && is_numeric($args['id'])) ? ($animals ? current($animals) : false) : $animals;
  }

  public function saveUserAnimal($data) {
    global $controller;
    $fields = array('id', 'name', 'images', 'kind', 'subspecies', 'born', 'description', 'user', 'date');
    $query = array('fields' => array(), 'values' => array(), 'update' => array());
    foreach ($fields as $field) {
      if (isset($data[$field])) {
        $query['fields'][] = $field;
        $query['values'][] = "'" . addslashes($data[$field]) . "'";
        if ($field != 'id') $query['update'][] = "$field = VALUES($field)";
      }
    }
    DB::connect()->query("INSERT INTO user_animals (" . implode(', ', $query['fields']) . ") VALUES (" . implode(', ', $query['values']) . ") ON DUPLICATE KEY UPDATE " . implode(', ', $query['update']));
    return DB::connect()->error ? false : (isset($data['id']) ? $data['id'] : DB::connect()->insert_id);
  }
}

?>
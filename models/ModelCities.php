<?php

class ModelCities extends Models {

  public function getCities($args = array()) {
    $cities = array();
    $result = DB::connect()->query("SELECT * FROM cities" . ($args ? ' WHERE ' . implode(' AND ', array_map(function ($k, $v) {return $k . (is_array($v) ? ' IN (' . implode(', ', array_map(function ($a) {return "'" . addslashes($a) . "'";}, $v)) . ')' : " = '" . addslashes($v) . "'");}, array_keys($args), $args)) : ''));
    while ($res = $result->fetch_assoc()) {
      $cities[$res['id']] = $res;
    }
    return (isset($args['id']) && is_numeric($args['id'])) ? ($cities ? current($cities) : false) : $cities;
  }
}

?>
<?php

class Controller {

  const DEFAULT_ANIMAL_IMAGE = '/css/img/default-image.png';

  public $userStatuses = array(1 => 'Active', 0 => 'Blocked');

  public $advertisementStatuses = array(1 => 'Active', 0 => 'Blocked');

  public $nurseryStatuses = array(1 => 'Active', 0 => 'Blocked');

  public $advertisementTypes = array(1 => 'Found', 2 => 'Lose');

  public $user = array('id' => 0);

  public $kinds = array(
    1 => array('name' => 'Bird', 'img' => '/css/img/kind-bird.png'),
    2 => array('name' => 'Cat', 'img' => '/css/img/kind-cat.png'),
    3 => array('name' => 'Dog', 'img' => '/css/img/kind-dog.png')
  );

  public $subspecies = array(
    1 => array('kind' => 1, 'name' => 'Val 1'),
    2 => array('kind' => 2, 'name' => 'Val 2'),
    3 => array('kind' => 3, 'name' => 'Val 3'),
  );

  public $password_salt = 'la2h3iqhd8q3';

  protected $siteUrl;

  public function __construct() {
    if (isset($_SESSION['user']) && is_numeric($_SESSION['user']) && $_SESSION['user'] > 0) {
      if (($result = DB::connect()->query("SELECT * FROM users WHERE id = " . $_SESSION['user'])->fetch_assoc()) && $result['status'] == 1) {
        $this->user = $result;
      }
      else unset($_SESSION['user']);
    }
    $this->siteUrl = 'http' . (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 's' : '') . '://' . $_SERVER['SERVER_NAME'];
  }

  public function getUser() {
    return $this->user;
  }

  public function indexAction() {
    return 'ok';
  }

  public function getSiteUrl() {
    return $this->siteUrl;
  }

  public function redirect($r) {
    header('Location: ' . $r);
    exit;
  }

  public function before() {
    
  }

  public function after() {
    
  }

  public function arg($num = 'all') {
    $url = explode('/', $this->getURL());
    return $num === 'all' ? $url : (array_key_exists($num, $url) ? $url[$num] : '');
  }

  public function getURL () {
    return preg_replace(array('/^\/*/', '/\?.*/'), '', $_SERVER['REQUEST_URI']);
  }

  public function json_for_js($arr) {
    return str_replace(array('\\', '\\\\u0022', '\u0022', "'"), array('\\\\', '\\u0022', '\\\"', "\'"), json_encode($arr, JSON_HEX_QUOT));
  }

  public function isValidEmail($e) {
    return preg_match('/^[\w-_.]+@[\w-_]+(\.[\w]{2,3}){1,2}$/', $e);
  }

  public function removeScripts($str) {
    while (preg_match('/(?:<script>|<\/script>)/', $str)) {
      if (preg_match('/<script[^>]*>[^<]*<\/script>/', $str)) $str = preg_replace('/<script[^>]*>[^<]*<\/script>/', '', $str);
      else $str = str_replace(array('<script>', '</script>'), '', $str);
    }
    return $str;
  }

  public function cutString($str, $len) {
    return strlen($str) <= $len ? $str : ((($pos = strrpos($substr = substr($str, 0, $len - 2), ' ')) ? substr($substr, 0, $pos) : substr($str, 0, $len - 3)) . '...');
  }

  public function setMessage($message, $type = 'success') {
    $_SESSION['messages'][$type][] = $message;
  }

  public function getMessages() {
    if (isset($_SESSION['messages']) && is_array($_SESSION['messages'])) {
      foreach ($_SESSION['messages'] as $type => $messages) {
        if (is_array($messages) && in_array($type, array('success', 'warning', 'error'))) {
          echo '<div class="message ' . $type . '">';
          foreach ($messages as $message) {
            echo '<div>' . $message . '</div>';
          }
          echo '</div>';
        }
      }
      unset($_SESSION['messages']);
    }
  }
}


?>
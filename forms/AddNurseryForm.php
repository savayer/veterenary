<?php

class AddNurseryForm extends Forms {

  public function render($args = array()) {
    global $controller;
    $this->beforeRender();
    $nursery = $_SERVER['REQUEST_METHOD'] == 'POST' ? $_POST : (($user_nurseries = Models::get('ModelNursery')->getNurseries(array('user' => $controller->user['id']))) ? current($user_nurseries) : array());
    ?>
    <form class="add-nursery-form" method="post">
      <?php if (isset($this->errors['other'])) : ?><div class="error"><?php echo $this->errors['other'] ?></div><?php endif; ?>
      <div>
        <input type="text" placeholder="שם*" name="name"<?php if (isset($nursery['name'])) : ?> value="<?php echo htmlspecialchars($nursery['name']) ?>"<?php endif; ?>>
        <?php if (isset($this->errors['name'])) : ?><div class="error"><?php echo $this->errors['name'] ?></div><?php endif; ?>
      </div>
      <div>
        <input type="text" placeholder="דואר אלקטרוני*" name="email"<?php if (isset($nursery['email'])) : ?> value="<?php echo htmlspecialchars($nursery['email']) ?>"<?php endif; ?>>
        <?php if (isset($this->errors['email'])) : ?><div class="error"><?php echo $this->errors['email'] ?></div><?php endif; ?>
      </div>
      <div>
        <input type="text" placeholder="טלפון*" name="phone"<?php if (isset($nursery['phone'])) : ?> value="<?php echo htmlspecialchars($nursery['phone']) ?>"<?php endif; ?>>
        <?php if (isset($this->errors['phone'])) : ?><div class="error"><?php echo $this->errors['phone'] ?></div><?php endif; ?>
      </div>
      <div>
        <input type="text" placeholder="כתובת*" name="address"<?php if (isset($nursery['address'])) : ?> value="<?php echo htmlspecialchars($nursery['address']) ?>"<?php endif; ?>>
        <?php if (isset($this->errors['address'])) : ?><div class="error"><?php echo $this->errors['address'] ?></div><?php endif; ?>
      </div>
      <div>
        <textarea name="description" placeholder="התיאור*"><?php if (isset($nursery['description'])) echo htmlspecialchars($nursery['description']); ?></textarea>
        <?php if (isset($this->errors['description'])) : ?><div class="error"><?php echo $this->errors['description'] ?></div><?php endif; ?>
      </div>
      <div>
        <a href="#" class="upload-image" data-function-pick="pickImage" data-function-upload="uploadImage" data-function-error="errorImage">הוסף תמונה</a>
        <div class="uploaded-image<?php if (isset($nursery['image'])) : ?> uploading uploaded" style="background-image:url(<?php echo CropImage::getImage($nursery['image']) ?>);background-size:cover;"<?php else : ?>"<?php endif; ?>>
          <a href="javascript:void(0)"></a>
          <input type="hidden" name="image"<?php if (isset($nursery['image'])) : ?> value="<?php echo htmlspecialchars($nursery['image']) ?>"<?php endif; ?>>
        </div>
      </div>
      <div class="mt-40">
        <input type="hidden" name="form_name" value="<?php echo __CLASS__ ?>">
        <input type="hidden" name="form_id" value="<?php echo $this->getFormId() ?>">
        <input type="submit" class="short-green-button" value="הרשמה">
      </div>
    </form>
  <?php }

  public function validate() {
    global $controller;
    if (!isset($_POST['name']) || !is_string($_POST['name']) || !$controller->removeScripts(strip_tags($_POST['name']))) {
      $this->setError('name', 'Name is required field');
    }
    if (!isset($_POST['email']) || !is_string($_POST['email']) || !$controller->removeScripts(strip_tags($_POST['email']))) {
      $this->setError('email', 'Email is required field');
    }
    elseif (!$controller->isValidEmail($_POST['email'])) {
      $this->setError('email', 'Invalid email');
    }
    if (!isset($_POST['phone']) || !is_string($_POST['phone']) || !preg_match('/^\d{10}$/', $_POST['phone'])) {
      $this->setError('phone', 'Wrong format');
    }
    if (!isset($_POST['address']) || !is_string($_POST['address']) || !$controller->removeScripts(strip_tags($_POST['address']))) {
      $this->setError('address', 'Address is required field');
    }
    if (!isset($_POST['description']) || !is_string($_POST['description']) || !$controller->removeScripts($_POST['address'])) {
      $this->setError('address', 'Description is required field');
    }
  }

  public function submit() {
    global $controller;
    if (($user_nurseries = Models::get('ModelNursery')->getNurseries(array('user' => $controller->user['id']))) && $nursery = current($user_nurseries)) {
      $_POST['id'] = $nursery['id'];
      $_POST['status'] = $nursery['status'];
    }
    else {
      if (isset($_POST['id'])) unset($_POST['id']);
      $_POST['status'] = 0;
    }
    $_POST['name'] = $controller->removeScripts(strip_tags($_POST['name']));
    $_POST['address'] = $controller->removeScripts(strip_tags($_POST['address']));
    $_POST['description'] = $controller->removeScripts($_POST['description']);
    $_POST['date'] = time();
    $_POST['user'] = $controller->user['id'];
    if (isset($_POST['image']) && is_string($_POST['image']) && file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/' . $_POST['image']))
      DB::connect()->query("DELETE FROM uploaded_files WHERE file = '" . $_POST['image'] . "'");
    else $_POST['image'] = '';
    Models::get('ModelNursery')->saveNursery($_POST);
    $controller->redirect('/nurseries');
  }
}

?>
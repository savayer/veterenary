<?php

class AdminForm extends Forms {

  public function render($args = array()) {
    if (isset($args['type']) && is_string($args['type']) && method_exists($this, $method = $args['type'] . 'Render')) {
      $this->beforeRender();
      $this->$method($args);
    }
  }

  public function validate() {
    if (isset($_POST['form_subname']) && is_string($_POST['form_subname']) && method_exists($this, $method = $_POST['form_subname'] . 'Validate')) $this->$method();
  }

  public function submit() {
    if (isset($_POST['form_subname']) && is_string($_POST['form_subname']) && method_exists($this, $method = $_POST['form_subname'] . 'Submit')) $this->$method();
  }

  public function usersRender($args) { global $controller; $user = $_SERVER['REQUEST_METHOD'] == 'POST' ? $_POST : $args['user']; ?>
    <form method="post" enctype="multipart/form-data">
      <table class="form-edit">
        <tbody>
          <tr>
            <th>Name: </th>
            <td>
              <input type="text" name="name"<?php if (isset($user['name'])) : ?> value="<?php echo htmlspecialchars($user['name']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['name'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['name']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Email: </th>
            <td>
              <input type="text" name="email"<?php if (isset($user['email'])) : ?> value="<?php echo htmlspecialchars($user['email']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['email'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['email']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Phone: </th>
            <td>
              <input type="text" name="phone"<?php if (isset($user['phone'])) : ?> value="<?php echo htmlspecialchars($user['phone']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['phone'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['phone']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Address: </th>
            <td>
              <input type="text" name="address"<?php if (isset($user['address'])) : ?> value="<?php echo htmlspecialchars($user['address']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['address'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['address']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Password: </th>
            <td>
              <input type="password" name="password">
              <?php if (isset($this->errors['password'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['password']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Status: </th>
            <td>
              <select name="status">
                <?php foreach ($controller->userStatuses as $k => $v) : ?>
                <option value="<?php echo $k ?>"<?php if (isset($user['status']) && $user['status'] == $k) : ?> selected<?php endif; ?>><?php echo htmlspecialchars($v) ?></option>
                <?php endforeach; ?>
              </select>
              <?php if (isset($this->errors['status'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['status']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th></th>
            <td>
              <?php if (isset($user['id'])) : ?><input type="hidden" name="id" value="<?php echo htmlspecialchars($user['id']) ?>"><?php endif; ?>
              <?php if (isset($this->errors['id'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['id']) ?></div><?php endif; ?>
              <input type="hidden" name="form_name" value="<?php echo __CLASS__ ?>">
              <input type="hidden" name="form_subname" value="<?php echo $args['type'] ?>">
              <input type="hidden" name="form_id" value="<?php echo $this->getFormId() ?>">
              <input type="submit" class="short-green-button" value="Submit">
            </td>
          </tr>
        </tbody>
      </table>
    </form>
  <?php }

  public function usersValidate() {
    global $controller;
    if (isset($_SESSION['admin_login'])) {
      if (!isset($_POST['name']) || !is_string($_POST['name']) || !$controller->removeScripts(strip_tags($_POST['name']))) {
        $this->setError('name', 'Name is required');
      }
      elseif (strlen($_POST['name']) > 255) {
        $this->setError('name', 'Length cannot be greater than 255');
      }
      if (!isset($_POST['email']) || !is_string($_POST['email']) || !$_POST['email']) {
        $this->setError('email', 'Email is required');
      }
      elseif (!$controller->isValidEmail($_POST['email'])) {
        $this->setError('email', 'Invalid email');
      }
      elseif (strlen($_POST['email']) > 255) {
        $this->setError('email', 'Length cannot be greater than 255');
      }
      elseif (($user = Models::get('ModelUsers')->getUsers(array('email' => $_POST['email']))) && (!isset($_POST['id']) || $_POST['id'] != $user['id'])) {
        $this->setError('email', 'This email already in use');
      }
      if (!isset($_POST['phone']) || !is_string($_POST['phone']) || !$_POST['phone']) {
        $this->setError('phone', 'Phone is required');
      }
      elseif (!preg_match('/^\d{10}$/', $_POST['phone'])) {
        $this->setError('phone', 'Wrong phone format');
      }
      if (!isset($_POST['address']) || !is_string($_POST['address']) || !$controller->removeScripts(strip_tags($_POST['address']))) {
        $this->setError('address', 'Address is required');
      }
      if (isset($_POST['id']) && (!is_numeric($_POST['id']) || !Models::get('ModelUsers')->getUsers(array('id' => $_POST['id'])))) {
        $this->setError('id', 'Invalid user');
      }
      elseif (isset($_POST['id']) && isset($_POST['password']) && !is_string($_POST['password'])) {
        $this->setError('password', 'Invalid password');
      }
      if (!isset($_POST['id']) && (!isset($_POST['password']) || !is_string($_POST['password']) || !$_POST['password'])) {
        $this->setError('password', 'Password is required');
      }
      if (!isset($_POST['status']) || !is_numeric($_POST['status']) || !array_key_exists($_POST['status'], $controller->userStatuses)) {
        $this->setError('status', 'Invalid status');
      }
    }
    else {
      $this->setError('global', 'You have no access');
    }
  }

  public function usersSubmit() {
    global $controller;
    $_POST['name'] = $controller->removeScripts(strip_tags($_POST['name']));
    $_POST['address'] = $controller->removeScripts(strip_tags($_POST['address']));
    if (isset($_POST['password']) && !$_POST['password']) unset($_POST['password']);
    Models::get('ModelUsers')->saveUser($_POST);
    $controller->redirect('/a-panel/users');
  }

  public function advertisementsRender($args) { global $controller; $advertisement = $_SERVER['REQUEST_METHOD'] == 'POST' ? $_POST : $args['advertisement']; ?>
    <form method="post" enctype="multipart/form-data">
      <table class="form-edit">
        <tbody>
          <tr>
            <th>Type: </th>
            <td>
              <select name="type">
                <?php foreach ($controller->advertisementTypes as $k => $v) : ?>
                <option value="<?php echo $k ?>"<?php if (isset($advertisement['type']) && $advertisement['type'] == $k) : ?> selected<?php endif; ?>><?php echo htmlspecialchars($v) ?></option>
                <?php endforeach; ?>
              </select>
              <?php if (isset($this->errors['type'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['type']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Images: </th>
            <td>
              <ul class="uploaded-images">
                <?php if (isset($advertisement['images'])) : foreach ($advertisement['images'] as $image) : ?>
                <li class="uploaded">
                  <a href="javascript:void(0)"></a>
                  <div style="background-image: url(<?php echo CropImage::getImage($image) ?>);background-size: cover;"></div>
                  <input type="hidden" name="images[]" value="<?php echo $image ?>">
                </li>
                <?php endforeach; endif; ?>
              </ul>
              <a href="#" class="upload-image" multiple="" data-function-pick="pickImages" data-function-upload="uploadImages" data-function-error="errorImages">Add image</a>
              <?php if (isset($this->errors['images'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['images']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>City: </th>
            <td>
              <select name="city">
                <option value="0">- Select -</option>
                <?php foreach (Models::get('ModelCities')->getCities() as $id => $city) : ?>
                <option value="<?php echo $id ?>"<?php if (isset($advertisement['city']) && $advertisement['city'] == $id) : ?> selected<?php endif; ?>><?php echo htmlspecialchars($city['name']) ?></option>
                <?php endforeach; ?>
              </select>
              <?php if (isset($this->errors['city'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['city']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Defined geo: </th>
            <td>
              <input type="text" name="geodefine"<?php if (isset($advertisement['geodefine'])) : ?> value="<?php echo htmlspecialchars($advertisement['geodefine']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['geodefine'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['geodefine']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Street: </th>
            <td>
              <input type="text" name="street"<?php if (isset($advertisement['street'])) : ?> value="<?php echo htmlspecialchars($advertisement['street']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['street'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['street']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>House number: </th>
            <td>
              <input type="text" name="house_number"<?php if (isset($advertisement['house_number'])) : ?> value="<?php echo htmlspecialchars($advertisement['house_number']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['house_number'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['house_number']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Date: </th>
            <td>
              <input type="text" class="custom-calendar" name="event_date"<?php if (isset($advertisement['event_date'])) : ?> value="<?php echo htmlspecialchars($advertisement['event_date']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['event_date'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['event_date']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Name: </th>
            <td>
              <input type="text" name="name"<?php if (isset($advertisement['name'])) : ?> value="<?php echo htmlspecialchars($advertisement['name']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['name'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['name']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Phone: </th>
            <td>
              <input type="text" name="phone"<?php if (isset($advertisement['phone'])) : ?> value="<?php echo htmlspecialchars($advertisement['phone']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['phone'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['phone']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Email: </th>
            <td>
              <input type="text" name="email"<?php if (isset($advertisement['email'])) : ?> value="<?php echo htmlspecialchars($advertisement['email']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['email'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['email']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Description: </th>
            <td>
              <textarea name="description"><?php if (isset($advertisement['description'])) echo htmlspecialchars($advertisement['description']); ?></textarea>
              <?php if (isset($this->errors['description'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['description']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Status: </th>
            <td>
              <select name="status">
                <?php foreach ($controller->advertisementStatuses as $k => $v) : ?>
                <option value="<?php echo $k ?>"<?php if (isset($advertisement['status']) && $advertisement['status'] == $k) : ?> selected<?php endif; ?>><?php echo htmlspecialchars($v) ?></option>
                <?php endforeach; ?>
              </select>
              <?php if (isset($this->errors['status'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['status']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th></th>
            <td>
              <?php if (isset($advertisement['id'])) : ?><input type="hidden" name="id" value="<?php echo htmlspecialchars($advertisement['id']) ?>"><?php endif; ?>
              <?php if (isset($this->errors['id'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['id']) ?></div><?php endif; ?>
              <input type="hidden" name="form_name" value="<?php echo __CLASS__ ?>">
              <input type="hidden" name="form_subname" value="<?php echo $args['type'] ?>">
              <input type="hidden" name="form_id" value="<?php echo $this->getFormId() ?>">
              <input type="submit" class="short-green-button" value="Submit">
            </td>
          </tr>
        </tbody>
      </table>
    </form>
  <?php }

  public function advertisementsValidate() {
    global $controller;
    if (isset($_SESSION['admin_login'])) {
      if (!isset($_POST['images']) || !is_array($_POST['images']) || !$_POST['images']) {
        $this->setError('images', 'Images is required');
      }
      else {
        try {
          foreach ($_POST['images'] as $img) {
            if (is_string($img) && file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/' . $img)) throw new Exception();
          }
          $this->setError('images', 'Images is required');
        }
        catch (Exception $ex) {}
      }
      if (!isset($_POST['description']) || !is_string($_POST['description']) || !$controller->removeScripts($_POST['description'])) {
        $this->setError('description', 'Description is required');
      }
      if (!isset($_POST['type']) || !is_string($_POST['type']) || !array_key_exists($_POST['type'], $controller->advertisementTypes)) {
        $this->setError('type', 'Type is required');
      }
      if (isset($_POST['city']) && (!is_numeric($_POST['city']) || $_POST['city'] < 0 || ($_POST['city'] > 0 && !Models::get('ModelCities')->getCities(array('id' => $_POST['city']))))) {
        $this->setError('city', 'Wrong city');
      }
      if (isset($_POST['street']) && (!is_string($_POST['street']) || ($_POST['street'] && !$controller->removeScripts(strip_tags($_POST['street']))))) {
        $this->setError('street', 'Wrong street');
      }
      if (isset($_POST['house_number']) && (!is_string($_POST['house_number']) || ($_POST['house_number'] && !$controller->removeScripts(strip_tags($_POST['house_number']))))) {
        $this->setError('house_number', 'Wrong house number');
      }
      if (isset($_POST['event_date']) && (!is_string($_POST['event_date']) || ($_POST['event_date'] && (!preg_match_all('/^(\d{1,2})\.(\d{1,2})\.(\d{4})$/', $_POST['event_date'], $date) || !checkdate($date[2][0], $date[1][0], $date[3][0]))))) {
        $this->setError('event_date', 'Wrong date');
      }
      if (isset($_POST['name']) && (!is_string($_POST['name']) || ($_POST['name'] && !$controller->removeScripts(strip_tags($_POST['name']))))) {
        $this->setError('name', 'Wrong name');
      }
      if (isset($_POST['phone']) && (!is_string($_POST['phone']) || ($_POST['phone'] && !preg_match('/^\d{10}$/', $_POST['phone'])))) {
        $this->setError('phone', 'Wrong phone');
      }
      if (isset($_POST['email']) && (!is_string($_POST['email']) || ($_POST['email'] && !$controller->isValidEmail($_POST['email'])))) {
        $this->setError('email', 'Wrong email');
      }
      if (!isset($_POST['status']) || !is_numeric($_POST['status']) || !array_key_exists($_POST['status'], $controller->advertisementStatuses)) {
        $this->setError('status', 'Wrong status');
      }
      if (isset($_POST['id']) && (!is_numeric($_POST['id']) || !Models::get('ModelAdvertisement')->getAdverts(array('id' => $_POST['id'])))) {
        $this->setError('id', 'Advertisements not found');
      }
    }
    else {
      $this->setError('global', 'You have no access');
    }
  }

  public function advertisementsSubmit() {
    global $controller;
    $images = array();
    foreach ($_POST['images'] as $img) {
      if (is_string($img) && file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/' . $img)) $images[] = $img;
    }
    DB::connect()->query("DELETE FROM uploaded_files WHERE file IN ('" . implode("', '", $images) . "')");
    $advertisements = array(
      'id' => isset($_POST['id']) ? $_POST['id'] : 0,
      'type' => $_POST['type'],
      'images' => implode(',', $images),
      'location' => json_encode(array(
                                'city' => isset($_POST['city']) ? $_POST['city'] : 0, 'street' => isset($_POST['street']) ? $controller->removeScripts(strip_tags($_POST['street'])) : '',
                                'house_number' => isset($_POST['house_number']) ? $controller->removeScripts(strip_tags($_POST['house_number'])) : '', 'event_date' => isset($_POST['event_date']) ? $_POST['event_date'] : ''
                              )),
      'user_data' => json_encode(array(
                                  'name' => isset($_POST['name']) ? $controller->removeScripts(strip_tags($_POST['name'])) : '', 'phone' => isset($_POST['phone']) ? $_POST['phone'] : '',
                                  'email' => isset($_POST['email']) ? $_POST['email'] : ''
                              )),
      'description' => $controller->removeScripts($_POST['description']),
      'status' => $_POST['status']
    );
    if (!isset($_POST['id'])) {
      $advertisements['date'] = time();
      $advertisements['user'] = $controller->user['id'];
    }
    Models::get('ModelAdvertisement')->saveAdvert($advertisements);
    $controller->redirect('/a-panel/advertisements');
  }

  public function nurseriesRender($args) { global $controller; $nursery = $_SERVER['REQUEST_METHOD'] == 'POST' ? $_POST : $args['nursery']; ?>
    <form method="post" enctype="multipart/form-data">
      <table class="form-edit">
        <tbody>
          <tr>
            <th>Name: </th>
            <td>
              <input type="text" name="name"<?php if (isset($nursery['name'])) : ?> value="<?php echo htmlspecialchars($nursery['name']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['name'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['name']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Email: </th>
            <td>
              <input type="text" name="email"<?php if (isset($nursery['email'])) : ?> value="<?php echo htmlspecialchars($nursery['email']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['email'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['email']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Phone: </th>
            <td>
              <input type="text" name="phone"<?php if (isset($nursery['phone'])) : ?> value="<?php echo htmlspecialchars($nursery['phone']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['phone'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['phone']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Address: </th>
            <td>
              <input type="text" name="address"<?php if (isset($nursery['address'])) : ?> value="<?php echo htmlspecialchars($nursery['address']) ?>"<?php endif; ?>>
              <?php if (isset($this->errors['address'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['address']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Description: </th>
            <td>
              <textarea name="description"><?php if (isset($nursery['description'])) echo htmlspecialchars($nursery['description']); ?></textarea>
              <?php if (isset($this->errors['description'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['description']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th>Image: </th>
            <td>
              <div class="uploaded-image<?php if (isset($nursery['image']) && $nursery['image']) : ?> uploading uploaded" style="background-image:url(<?php echo CropImage::getImage($nursery['image']) ?>);background-size:cover;"<?php else : ?>"<?php endif; ?>>
                <a href="javascript:void(0)"></a>
                <input type="hidden" name="image"<?php if (isset($nursery['image'])) : ?> value="<?php echo $nursery['image'] ?>"<?php endif; ?>>
              </div>
              <a href="#" class="upload-image" data-function-pick="pickImage" data-function-upload="uploadImage" data-function-error="errorImage">Add image</a>
            </td>
          </tr>
          <tr>
            <th>Status: </th>
            <td>
              <select name="status">
                <?php foreach ($controller->nurseryStatuses as $k => $v) : ?>
                <option value="<?php echo $k ?>"<?php if (isset($nursery['status']) && $nursery['status'] == $k) : ?> selected<?php endif; ?>><?php echo htmlspecialchars($v) ?></option>
                <?php endforeach; ?>
              </select>
              <?php if (isset($this->errors['status'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['status']) ?></div><?php endif; ?>
            </td>
          </tr>
          <tr>
            <th></th>
            <td>
              <?php if (isset($nursery['id'])) : ?><input type="hidden" name="id" value="<?php echo htmlspecialchars($nursery['id']) ?>"><?php endif; ?>
              <?php if (isset($this->errors['id'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['id']) ?></div><?php endif; ?>
              <input type="hidden" name="form_name" value="<?php echo __CLASS__ ?>">
              <input type="hidden" name="form_subname" value="<?php echo $args['type'] ?>">
              <input type="hidden" name="form_id" value="<?php echo $this->getFormId() ?>">
              <input type="submit" class="short-green-button" value="Submit">
            </td>
          </tr>
        </tbody>
      </table>
    </form>
  <?php }

  public function nurseriesValidate() {
    global $controller;
    if (!isset($_POST['name']) || !is_string($_POST['name']) || !$controller->removeScripts(strip_tags($_POST['name']))) {
      $this->setError('name', 'Name is required field');
    }
    if (!isset($_POST['email']) || !is_string($_POST['email']) || !$controller->removeScripts(strip_tags($_POST['email']))) {
      $this->setError('email', 'Email is required field');
    }
    elseif (!$controller->isValidEmail($_POST['email'])) {
      $this->setError('email', 'Invalid email');
    }
    if (!isset($_POST['phone']) || !is_string($_POST['phone']) || !preg_match('/^\d{10}$/', $_POST['phone'])) {
      $this->setError('phone', 'Wrong format');
    }
    if (!isset($_POST['address']) || !is_string($_POST['address']) || !$controller->removeScripts(strip_tags($_POST['address']))) {
      $this->setError('address', 'Address is required field');
    }
    if (!isset($_POST['status']) || !is_numeric($_POST['status']) || !array_key_exists($_POST['status'], $controller->nurseryStatuses)) {
      $this->setError('status', 'Invalid status');
    }
    if (isset($_POST['id']) && (!is_numeric($_POST['id']) || !Models::get('ModelNursery')->getNurseries(array('id' => $_POST['id'])))) {
      $this->setError('id', 'Invalid nursery');
    }
  }

  public function nurseriesSubmit() {
    global $controller;
    $_POST['name'] = $controller->removeScripts(strip_tags($_POST['name']));
    $_POST['address'] = $controller->removeScripts(strip_tags($_POST['address']));
    if (!isset($_POST['id'])) {
      $_POST['date'] = time();
      $_POST['user'] = $controller->user['id'];
    }
    if (isset($_POST['description'])) $_POST['description'] = $controller->removeScripts($_POST['description']);
    if (isset($_POST['image']) && is_string($_POST['image']) && file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/' . $_POST['image']))
      DB::connect()->query("DELETE FROM uploaded_files WHERE file = '" . $_POST['image'] . "'");
    else $_POST['image'] = '';
    Models::get('ModelNursery')->saveNursery($_POST);
    $controller->redirect('/a-panel/nurseries');
  }
}

?>
<?php

class AddUserAnimal extends Forms {

  public function render($args = array()) {
    global $controller;
    if ($_SERVER['REQUEST_METHOD'] == 'POST') $args = $_POST;
    $this->beforeRender(); ?>
    <form method="post">
      <?php if (isset($this->errors['global'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['global']) ?></div><?php endif; ?>
      <div id="make-photo-wrapper">
        <div class="add-image-wrapper circle-wrapper">
          <div class="add-image circle" style="background-image: url(<?php echo Controller::DEFAULT_ANIMAL_IMAGE ?>);">
            <video id="camera-video" style="width: 214px; height: auto;"></video>
          </div>
        </div>
      </div>
      <a href="#" class="upload-image" multiple="" data-function-pick="pickImages" data-function-upload="uploadImages" data-function-error="errorImages">Add image</a>
      <div class="animal-data">
        <ul class="uploaded-images mt-20 ta-right">
          <?php if (isset($args['images']) && is_array($args['images'])) : foreach ($args['images'] as $image) : ?>
          <li class="uploaded">
            <a href="javascript:void(0)"></a>
            <div style="background-image:url(<?php echo CropImage::getImage($image) ?>);background-size:cover"></div>
            <input type="hidden" name="images[]" value="<?php echo $image ?>"></li>
          <?php endforeach; endif; ?>
        </ul>
        <?php if (isset($this->errors['images'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['images']) ?></div><?php endif; ?>
        <h3>Some text</h3>
        <div class="animal-kind">
          <?php foreach ($controller->kinds as $k => $v) : ?>
          <label>
            <input type="radio" name="kind" value="<?php echo $k ?>" required<?php if (isset($args['kind']) && $args['kind'] == $k) : ?> checked<?php endif; ?>>
            <span style="background-image: url(<?php echo $v['img'] ?>)"></span>
          </label>
          <?php endforeach; ?>
        </div>
        <?php if (isset($this->errors['kind'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['kind']) ?></div><?php endif; ?>
        <h3>Animal birthday</h3>
        <div class="animal-birthday">
          <select name="year">
            <?php for ($year = date('Y'), $y = $year; $y >= $year - 20; $y--) : ?>
            <option value="<?php echo $y ?>"<?php if (isset($args['year']) && $args['year'] == $y) : ?> selected<?php endif; ?>><?php echo $y ?></option>
            <?php endfor; ?>
          </select>
          <select name="month">
            <?php for ($m = 1; $m <= 12; $m++) : ?>
            <option value="<?php echo $m ?>"<?php if (isset($args['month']) && $args['month'] == $m) : ?> selected<?php endif; ?>><?php echo ($m < 10 ? '0' : '') . $m ?></option>
            <?php endfor; ?>
          </select>
          <select name="day">
            <?php for ($d = 1; $d <= 31; $d++) : ?>
            <option value="<?php echo $d ?>"<?php if (isset($args['day']) && $args['day'] == $d) : ?> selected<?php endif; ?>><?php echo ($d < 10 ? '0' : '') . $d ?></option>
            <?php endfor; ?>
          </select>
        </div>
        <?php if (isset($this->errors['date'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['date']) ?></div><?php endif; ?>
        <h3>Animal name</h3>
        <div>
          <input class="w-100p" type="text" name="name" required<?php if (isset($args['name'])) : ?> value="<?php echo htmlspecialchars($args['name']) ?>"<?php endif; ?>>
          <?php if (isset($this->errors['name'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['name']) ?></div><?php endif; ?>
        </div>
        <div class="mt-20">
          <textarea name="description" placeholder="Description"><?php if (isset($args['description'])) echo htmlspecialchars($args['description']) ?></textarea>
        </div>
        <h3 class="animal-subspecies-select"<?php if (isset($args['kind'])) : ?> style="display:block;"<?php endif; ?>>Some select</h3>
        <div class="long-select animal-subspecies-select"<?php if (isset($args['kind'])) : ?> style="display:block;"<?php endif; ?>>
          <select name="subspecies">
            <?php foreach ($controller->subspecies as $k => $v) : ?>
            <option value="<?php echo $k ?>" kind="<?php echo $v['kind'] ?>"<?php if (!isset($args['kind']) || $args['kind'] != $v['kind']) : ?> style="display:none;"<?php endif; ?><?php if (isset($args['subspecies']) && $args['subspecies'] == $k) : ?> selected<?php endif; ?>><?php echo htmlspecialchars($v['name']) ?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <?php if (isset($this->errors['subspecies'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['subspecies']) ?></div><?php endif; ?>
      </div>
      <div class="scrobler"></div>
      <?php if (isset($args['id'])) : ?><input type="hidden" name="id" value="<?php echo $args['id'] ?>"><?php endif; ?>
      <input type="hidden" name="form_name" value="<?php echo __CLASS__ ?>">
      <input type="hidden" name="form_id" value="<?php echo $this->getFormId() ?>">
      <?php if (isset($this->errors['id'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['id']) ?></div><?php endif; ?>
      <input type="submit" class="long-green-button mt-20" value="Submit">
    </form>
  <?php }

  public function validate() {
    global $controller;
    if (!$controller->user['id']) {
      $this->setError('global', 'You have not access');
    }
    else {
      if (isset($_POST['id']) && (!is_numeric($_POST['id']) || !($animal = Models::get('ModelAnimals')->getUserAnimals(array('id' => $_POST['id']))) || $animal['user'] != $controller->user['id'])) {
        $this->setError('id', 'Wrong animal');
      }
      if (isset($_POST['images']) && !is_array($_POST['images'])) {
        $this->setError('images', 'Wrong images');
      }
      if (!isset($_POST['kind']) || !is_numeric($_POST['kind']) || !array_key_exists($_POST['kind'], $controller->kinds)) {
        $this->setError('kind', 'Wrong kind');
      }
      elseif (!isset($_POST['subspecies']) || !is_numeric($_POST['subspecies']) || !array_key_exists($_POST['subspecies'], $controller->subspecies) || $controller->subspecies[$_POST['subspecies']]['kind'] != $_POST['kind']) {
        $this->setError('subspecies', 'Wrong subspecies');
      }
      if (!isset($_POST['year']) || !is_numeric($_POST['year']) || !isset($_POST['month']) || !is_numeric($_POST['month']) ||
      !isset($_POST['day']) || !is_numeric($_POST['day']) || !checkdate($_POST['month'], $_POST['day'], $_POST['year'])) {
        $this->setError('date', 'Wrong date');
      }
      if (!isset($_POST['name']) || !is_string($_POST['name']) || !$controller->removeScripts(strip_tags($_POST['name']))) {
        $this->setError('name', 'Name is required');
      }
    }
  }

  public function submit() {
    global $controller;
    if (isset($_POST['images'])) {
      foreach ($_POST['images'] as $k => $image) {
        if (!is_string($image) || !file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/' . $image)) unset($_POST['images'][$k]);
      }
      if ($_POST['images']) DB::connect()->query("DELETE FROM uploaded_files WHERE file IN ('" . implode("', '", $_POST['images']) . "')");
      $_POST['images'] = implode(',', $_POST['images']);
    }
    else {
      $_POST['images'] = '';
    }
    $_POST['born'] = ($_POST['day'] < 10 ? '0' . $_POST['day'] : $_POST['day']) . '.' . ($_POST['month'] < 10 ? '0' . $_POST['month'] : $_POST['month']) . '.' . $_POST['year'];
    $_POST['description'] = isset($_POST['description']) && is_string($_POST['description']) ? $controller->removeScripts($_POST['description']) : '';
    $_POST['name'] = $controller->removeScripts(strip_tags($_POST['name']));
    if (isset($_POST['id'])) {
      unset($_POST['date'], $_POST['user']);
    }
    else {
      $_POST['date'] = time();
      $_POST['user'] = $controller->user['id'];
    }
    Models::get('ModelAnimals')->saveUserAnimal($_POST);
    $controller->redirect('/user-animals');
  }
}

?>
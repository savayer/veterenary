<?php

class AddAdvertForm extends Forms {

  public function render($args = array()) {
    if ($_SERVER['REQUEST_METHOD'] == 'POST') $args = $_POST;
    $this->beforeRender(); ?>
    
    <form method="post">
      <div class="edit-animal-wrapper">
        <div class="edit-animal-image"<?php if (isset($args['image'])) : ?> style="background-image:url(<?php echo $args['image'] ?>);background-size:cover;"<?php endif; ?>>
          <div class="edit-animal-image-text">הוסף קריאה</div>
          <div class="edit-animal-image--upload-image" multiple data-function-pick="pickImages" data-function-upload="uploadImages" data-function-error="errorImages"></div>
          
          <div class="camera">
            <video id="camera-stream" muted></video>
            <img id="snap">

            <p id="error-message"></p>
            <select id="cameras" style="display: none"></select>
            <div class="controls">
              <a href="#" id="close_camera" title="close camera"><i class="material-icons">close</i></a>
              <!-- <a href="#" id="switch_camera" title="switch camera"><i class="material-icons">switch_camera</i></a> -->
              <a href="#" id="download-photo" title="Done" class="disabled"><i class="material-icons">done</i></a>
              <a href="#" id="take-photo" title="Take Photo"><i class="material-icons">camera_alt</i></a>
              <a href="#" id="delete-photo" title="Delete Photo" class="disabled"><i class="material-icons">delete</i></a>
            </div>
            <canvas></canvas>
          </div>

          <a href="#" class="upload-image" multiple data-function-pick="pickImages" data-function-upload="uploadImages" data-function-error="errorImages">Add image</a>
          <label>
            <input type="radio" name="type" value="1"<?php if (isset($args['type']) && $args['type'] == '1') : ?> checked<?php endif; ?>>
            <div class="button2"></div>
          </label>
          <label>
            <input type="radio" name="type" value="2"<?php if (isset($args['type']) && $args['type'] == '2') : ?> checked<?php endif; ?>>
            <div class="button1"></div>
          </label>
        </div>
        <ul class="uploaded-images">
          <?php if (isset($_POST['images']) && is_array($_POST['images'])) : foreach ($_POST['images'] as $image) : ?>
          <li class="uploaded">
            <a href="javascript:void(0)"></a>
            <div style="background-image: url(/files/<?php echo $image ?>); background-size: cover;"></div>
            <input type="hidden" name="images[]" value="<?php echo $image ?>"></li>
          <?php endforeach; endif; ?>
        </ul>
        <?php if (isset($this->errors['type'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['type']) ?></div><?php endif; ?>
        <?php if (isset($this->errors['images'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['images']) ?></div><?php endif; ?>
        <div class="ta-left">
          <a href="javascript:void(0)" class="pin geolocate edit-animal c-ae5daa fs-18 "></a>
          <img src="/images/gif-preloader.gif" id="preloader-image" width="25" alt="" style="position: relative;left: 100px;">
        </div>
        <h3>Some text</h3>
        <div class="selects mb-10">
          <div class="inline-block va-top mb-10">
            <select name="city">
              <option value="0">- Select - </option>
              <?php foreach (Models::get('ModelCities')->getCities() as $city) : ?>
              <option value="<?php echo $city['id'] ?>"<?php if (isset($args['city']) && $args['city'] == $city['id']) : ?> selected<?php endif; ?>><?php echo htmlspecialchars($city['name']); ?></option>
              <?php endforeach; ?>
            </select>
            <?php if (isset($this->errors['city'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['city']) ?></div><?php endif; ?>
          </div>
          <div class="inline-block va-top mb-10">
            <input type="text" name="street" placeholder="Street"<?php if (isset($args['street'])) : ?> value="<?php echo htmlspecialchars($args['street']) ?>"<?php endif; ?>>
            <?php if (isset($this->errors['street'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['street']) ?></div><?php endif; ?>
          </div>
          <div class="inline-block va-top mb-10">
            <input type="text" name="house_number" placeholder="House number"<?php if (isset($args['house_number'])) : ?> value="<?php echo htmlspecialchars($args['house_number']) ?>"<?php endif; ?>>
            <?php if (isset($this->errors['house_number'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['house_number']) ?></div><?php endif; ?>
          </div>
          <div class="inline-block va-top mb-10">
            <input type="text" name="event_date" class="custom-calendar" placeholder="Date"<?php if (isset($args['event_date'])) : ?> value="<?php echo htmlspecialchars($args['event_date']) ?>"<?php else: ?> value="<?php echo date('d.m.Y'); endif; ?>" >
            <?php if (isset($this->errors['event_date'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['event_date']) ?></div><?php endif; ?>
          </div>
        </div>
        <div class="mb-10">
          <h3>User data</h3>
          <div class="inline-block va-top mb-5">
            <input type="text" name="name" placeholder="Name"<?php if (isset($args['name'])) : ?> value="<?php echo htmlspecialchars($args['name']) ?>"<?php endif; ?>>
            <?php if (isset($this->errors['name'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['name']) ?></div><?php endif; ?>
          </div>
          <div class="inline-block va-top mb-5">
            <input type="text" name="phone" placeholder="Phone"<?php if (isset($args['phone'])) : ?> value="<?php echo htmlspecialchars($args['phone']) ?>"<?php endif; ?>>
            <?php if (isset($this->errors['phone'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['phone']) ?></div><?php endif; ?>
          </div>
          <div class="inline-block va-top mb-5">
            <input type="text" name="email" placeholder="Email"<?php if (isset($args['email'])) : ?> value="<?php echo htmlspecialchars($args['email']) ?>"<?php endif; ?>>
            <?php if (isset($this->errors['email'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['email']) ?></div><?php endif; ?>
          </div>
        </div>
        <h3>Some text</h3>
        <div>
          <textarea name="description" placeholder="Description"><?php if (isset($args['description'])) echo htmlspecialchars($args['description']) ?></textarea>
          <?php if (isset($this->errors['description'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['description']) ?></div><?php endif; ?>
        </div>
        <label class="checkbox-label">
          <input type="checkbox" name="agree">
          <h3>Some text</h3>
          <?php if (isset($this->errors['agree'])) : ?><div class="error"><?php echo htmlspecialchars($this->errors['agree']) ?></div><?php endif; ?>
        </label>
        <input type="hidden" name="geodefine" value="">
        <input type="hidden" name="form_name" value="<?php echo __CLASS__ ?>">
        <input type="hidden" name="form_id" value="<?php echo $this->getFormId() ?>">
        <input type="submit" class="long-green-button mt-20" value="Submit">
      </div>
    </form>
  <?php }

  public function validate() {
    global $controller;
    if (!$controller->user['id']) {
      $this->setError('global', 'You have not rights');
    }
    else {
      if (!isset($_POST['images']) || !is_array($_POST['images']) || !$_POST['images']) {
        $this->setError('images', 'Images is required');
      }
      else {
        try {
          foreach ($_POST['images'] as $img) {
            if (is_string($img) && file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/' . $img)) throw new Exception();
          }
          $this->setError('images', 'Images is required');
        }
        catch (Exception $ex) {}
      }
      if (!isset($_POST['description']) || !is_string($_POST['description']) || !$controller->removeScripts($_POST['description'])) {
        $this->setError('description', 'Description is required');
      }
      if (!isset($_POST['type']) || !is_string($_POST['type']) || !array_key_exists($_POST['type'], $controller->advertisementTypes)) {
        $this->setError('type', 'Type is required');
      }
      if (isset($_POST['city']) && (!is_numeric($_POST['city']) || $_POST['city'] < 0 || ($_POST['city'] > 0 && !Models::get('ModelCities')->getCities(array('id' => $_POST['city']))))) {
        $this->setError('city', 'Wrong city');
      }
      if (isset($_POST['street']) && (!is_string($_POST['street']) || ($_POST['street'] && !$controller->removeScripts(strip_tags($_POST['street']))))) {
        $this->setError('street', 'Wrong street');
      }
      if (isset($_POST['house_number']) && (!is_string($_POST['house_number']) || ($_POST['house_number'] && !$controller->removeScripts(strip_tags($_POST['house_number']))))) {
        $this->setError('house_number', 'Wrong house number');
      }
      if (isset($_POST['event_date']) && (!is_string($_POST['event_date']) || ($_POST['event_date'] && (!preg_match_all('/^(\d{1,2})\.(\d{1,2})\.(\d{4})$/', $_POST['event_date'], $date) || !checkdate($date[2][0], $date[1][0], $date[3][0]))))) {
        $this->setError('event_date', 'Wrong date');
      }
      if (isset($_POST['name']) && (!is_string($_POST['name']) || ($_POST['name'] && !$controller->removeScripts(strip_tags($_POST['name']))))) {
        $this->setError('name', 'Wrong name');
      }
      if (isset($_POST['phone']) && (!is_string($_POST['phone']) || ($_POST['phone'] && !preg_match('/^\d{10}$/', $_POST['phone'])))) {
        $this->setError('phone', 'Wrong phone');
      }
      if (isset($_POST['email']) && (!is_string($_POST['email']) || ($_POST['email'] && !$controller->isValidEmail($_POST['email'])))) {
        $this->setError('email', 'Wrong email');
      }
      if (!isset($_POST['agree'])) {
        $this->setError('agree', 'Need to agree');
      }
    }
  }

  public function submit() {
    global $controller;
    $images = array();
    foreach ($_POST['images'] as $img) {
      if (is_string($img) && file_exists($_SERVER['DOCUMENT_ROOT'] . '/files/' . $img)) $images[] = $img;
    }
    DB::connect()->query("DELETE FROM uploaded_files WHERE file IN ('" . implode("', '", $images) . "')");
    Models::get('ModelAdvertisement')->saveAdvert(array(
      'id' => 0,
      'type' => $_POST['type'],
      'images' => implode(',', $images),
      'location' => json_encode(array(
                                'city' => isset($_POST['city']) ? $_POST['city'] : 0, 'street' => isset($_POST['street']) ? $controller->removeScripts(strip_tags($_POST['street'])) : '',
                                'house_number' => isset($_POST['house_number']) ? $controller->removeScripts(strip_tags($_POST['house_number'])) : '', 'event_date' => isset($_POST['event_date']) ? $_POST['event_date'] : ''
                              )),
      'geodefine' => $_POST['geodefine'],
      'user_data' => json_encode(array(
                                  'name' => isset($_POST['name']) ? $controller->removeScripts(strip_tags($_POST['name'])) : '', 'phone' => isset($_POST['phone']) ? $_POST['phone'] : '',
                                  'email' => isset($_POST['email']) ? $_POST['email'] : ''
                              )),
      'description' => $controller->removeScripts($_POST['description']),
      'date' => strtotime($_POST['event_date']),//time(),
      'user' => $controller->user['id'],
      'status' => 0
    ));
    $controller->redirect('/');
  }
}

?>
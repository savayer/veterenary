<?php

class LoginRegisterForm extends Forms {

  public function render($args = array()) {
    if (isset($args['type']) && is_string($args['type']) && method_exists($this, $method = $args['type'] . 'Render')) {
      $this->beforeRender();
      $this->$method();
    }
  }

  public function validate() {
    if (isset($_POST['form_subname']) && is_string($_POST['form_subname']) && method_exists($this, $method = $_POST['form_subname'] . 'Validate')) $this->$method();
  }

  public function submit() {
    if (isset($_POST['form_subname']) && is_string($_POST['form_subname']) && method_exists($this, $method = $_POST['form_subname'] . 'Submit')) $this->$method();
  }

  public function loginRender() { ?>
    <form class="login-form" method="post">
      <div>
        <input type="text" placeholder="דואר אלקטרוני*" name="email"<?php if (isset($_POST['email'])) : ?> value="<?php echo htmlspecialchars($_POST['email']); ?>"<?php endif; ?>>
        <?php if (isset($this->errors['email'])) : ?><div class="error"><?php echo $this->errors['email'] ?></div><?php endif; ?>
      </div>
      <div>
        <input type="password" placeholder="סיסמה*" name="password">
        <?php if (isset($this->errors['password'])) : ?><div class="error"><?php echo $this->errors['password'] ?></div><?php endif; ?>
      </div>
      <div>
        <input type="hidden" name="form_name" value="<?php echo __CLASS__ ?>">
        <input type="hidden" name="form_subname" value="login">
        <input type="hidden" name="form_id" value="<?php echo $this->getFormId() ?>">
        <input type="submit" class="short-green-button" value="כניסה">
      </div>
    </form>
  <?php }

  public function loginValidate() {
    global $controller;
    try {
      if (!isset($_POST['email']) || !is_string($_POST['email']) || !$_POST['email']) {
        $this->setError('email', 'Email is required');
        throw new Exception();
      }
      if (!isset($_POST['password']) || !is_string($_POST['password']) || !$_POST['password']) {
        $this->setError('password', 'Password is required');
      }
      elseif (!($user = Models::get('ModelUsers')->getUsers(array('email' => $_POST['email']))) || $user['password'] != md5($_POST['password'] . $controller->password_salt)) {
        $this->setError('email', 'Wrong email or password');
      }
      elseif ($user['status'] != 1) {
        $this->setError('email', 'This user is blocked');
      }
    }
    catch (Exception $ex) {
      if (!isset($_POST['password']) || !is_string($_POST['password']) || !$_POST['password']) {
        $this->setError('password', 'Password is required');
      }
    }
  }

  public function loginSubmit() {
    global $controller;
    $user = Models::get('ModelUsers')->getUsers(array('email' => $_POST['email']));
    $_SESSION['user'] = $user['id'];
    $controller->redirect('/');
  }

  public function registerRender() { ?>
    <form class="login-form" method="post">
      <?php if (isset($this->errors['other'])) : ?><div class="error"><?php echo $this->errors['other'] ?></div><?php endif; ?>
      <div>
        <input type="text" placeholder="שם*" name="name"<?php if (isset($_POST['name'])) : ?> value="<?php echo htmlspecialchars($_POST['name']) ?>"<?php endif; ?>>
        <?php if (isset($this->errors['name'])) : ?><div class="error"><?php echo $this->errors['name'] ?></div><?php endif; ?>
      </div>
      <div>
        <input type="text" placeholder="דואר אלקטרוני*" name="email"<?php if (isset($_POST['email'])) : ?> value="<?php echo htmlspecialchars($_POST['email']) ?>"<?php endif; ?>>
        <?php if (isset($this->errors['email'])) : ?><div class="error"><?php echo $this->errors['email'] ?></div><?php endif; ?>
      </div>
      <div>
        <input type="text" placeholder="טלפון*" name="phone"<?php if (isset($_POST['phone'])) : ?> value="<?php echo htmlspecialchars($_POST['phone']) ?>"<?php endif; ?>>
        <?php if (isset($this->errors['phone'])) : ?><div class="error"><?php echo $this->errors['phone'] ?></div><?php endif; ?>
      </div>
<!--      <div>
        <input type="text" placeholder="כתובת*" name="address"<?php //if (isset($_POST['address'])) : ?> value="<?php //echo htmlspecialchars($_POST['address']) ?>"<?php //endif; ?>>
        <?php //if (isset($this->errors['address'])) : ?><div class="error"><?php //echo $this->errors['address'] ?></div><?php //endif; ?>
      </div>-->
      <div>
        <input type="password" placeholder="סיסמה*" name="password">
        <?php if (isset($this->errors['password'])) : ?><div class="error"><?php echo $this->errors['password'] ?></div><?php endif; ?>
      </div>
      <div class="mt-40">
        <input type="hidden" name="form_name" value="<?php echo __CLASS__ ?>">
        <input type="hidden" name="form_subname" value="register">
        <input type="hidden" name="form_id" value="<?php echo $this->getFormId() ?>">
        <input type="submit" class="short-green-button" value="הרשמה">
      </div>
    </form>
  <?php }

  public function registerValidate() {
    global $controller;
    if (!isset($_POST['name']) || !is_string($_POST['name']) || !$controller->removeScripts(strip_tags($_POST['name']))) {
      $this->setError('name', 'Name is required');
    }
    elseif (strlen($_POST['name']) > 255) {
      $this->setError('name', 'Length cannot be greater than 255');
    }
    if (!isset($_POST['email']) || !is_string($_POST['email']) || !$_POST['email']) {
      $this->setError('email', 'Email is required');
    }
    elseif (!$controller->isValidEmail($_POST['email'])) {
      $this->setError('email', 'Invalid email');
    }
    elseif (strlen($_POST['email']) > 255) {
      $this->setError('email', 'Length cannot be greater than 255');
    }
    elseif (Models::get('ModelUsers')->getUsers(array('email' => $_POST['email']))) {
      $this->setError('email', 'This email already in use');
    }
    if (!isset($_POST['phone']) || !is_string($_POST['phone']) || !$_POST['phone']) {
      $this->setError('phone', 'Phone is required');
    }
    elseif (!preg_match('/^\d{10}$/', $_POST['phone'])) {
      $this->setError('phone', 'Wrong phone format');
    }
    //if (!isset($_POST['address']) || !is_string($_POST['address']) || !$controller->removeScripts(strip_tags($_POST['address']))) {
    //  $this->setError('address', 'Address is required');
    //}
    if (!isset($_POST['password']) || !is_string($_POST['password']) || !$_POST['password']) {
      $this->setError('password', 'Password is required');
    }
  }

  public function registerSubmit() {
    global $controller;
    $_POST['name'] = $controller->removeScripts(strip_tags($_POST['name']));
    $_POST['address'] = /*$controller->removeScripts(strip_tags($_POST['address']))*/'';
    $_POST['status'] = 1;
    if (isset($_POST['id'])) unset($_POST['id']);
    if ($id = Models::get('ModelUsers')->saveUser($_POST)) {
      $_SESSION['user'] = $id;
      $controller->redirect('/add-advert');
    }
    else {
      $controller->setMessage('Something wrong', 'error');
      $controller->redirect('/register');
    }
  }
}

?>